package com.liwenhao.test20211101;

public class CashCow {
	private int height=0;
	private int goldPiece=0;
	private boolean wormy=false;
	private int pesticide=0;
	public void grow(){
	    // 方法执行一次，摇钱树成长一次，高度增加 2 ，金币数量增加 10
		
		height+=2;
		goldPiece+=10;
		System.out.println("摇钱树长大了，高度加2，金币加10");
	}
	public void buy(){
	    // 当有金币时，从树上采摘金币购买农药
	    // 每购买一次农药，金币减少 1 个，杀虫剂增加 100
		if(goldPiece!=0) {
			goldPiece-=1;
			pesticide+=100;
			System.out.println("购买杀虫剂成功，金币减1，杀虫剂+100");
		}else {
			System.out.println("钱不够");
		}
	}
	public void kill(){
	    // 当有虫子时，执行杀虫操作
	    // 每执行一次杀虫操作，杀虫剂减少 50 ， 金币增加 5 个
		if(wormy) {
			if(pesticide>=50) {
				pesticide-=50;
				goldPiece+=5;
				wormy=false;
				System.out.println("杀虫成功，杀虫剂减50，金币加5，虫子消失");
			}else {
				System.out.println("杀虫剂不够");
			}
			
		}else {
			System.out.println("没有虫子");
		}
	}
	public void watering(){
	    // 每次浇水，金币数量增加 5 个，高度增加 1
	    // 每次浇水都会导致虫子增加 ( 即 wormy 变为 true )
		goldPiece+=5;
		height+=1;
		wormy=true;
		System.out.println("浇水成功，金币加5，高度加1，虫子增加");
	}
	public void show(){
	    // 通过 show 方法显示 摇钱树的高度 、摇钱树上的金币数量 、是否用虫子 、杀虫剂数量
		System.out.println("摇钱树高度    摇钱树金币数量    是否有虫子    杀虫剂数量");
		System.out.println(height+"           "+goldPiece+"             "+wormy+"        "+pesticide);
	}
}
