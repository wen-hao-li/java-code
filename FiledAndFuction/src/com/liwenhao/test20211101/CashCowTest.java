package com.liwenhao.test20211101;

import java.util.Random;

public class CashCowTest {

	public static void main(String[] args) {

		Random random = new Random(); // 创建一个随机数生成器

		CashCow cc = new CashCow(); // 创建摇钱树实例(该实例内部包含4个字段)

		for (int i = 0; i < 10; i++) {

			// 随机产生一个 [ 0 , 4 ) 之间的整数
			int x = random.nextInt(4); // 除了 show 仅有 4 个操作

			switch (x) {
			case 0:
				cc.grow();
				break;
			case 1:
				cc.buy();
				break;
			case 2:
				cc.kill();
				break;
			case 3:
				cc.watering();
				break;
			}
		}

		cc.show(); // 查询状态

	}

}
