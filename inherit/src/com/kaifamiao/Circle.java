package com.kaifamiao;

public class Circle extends Shape{
	protected double radius;

	public Circle() {
		super();
		this.type="圆";
	}
	public void calculate() {
		this.area=Math.PI*Math.pow(radius, 2);
	}
	public void description() {
		System.out.println(this.type+"的半径是"+this.radius);
		this.show();
	}
}
