package com.kaifamiao.obj;



public class Sector extends Shape{
	protected double radius;
	protected double arcLength;
	public Sector( double radius, double arcLength) {
		super("扇形");
		this.radius = radius;
		this.arcLength = arcLength;
	}
	public void calculate() {
		super.calculate();
		this.area=this.radius*this.arcLength/2;
	}
	public String toString() {
		String msg="半径为"+this.radius+"弧长为"+this.arcLength;
		return super.toString()+","+msg;
	}
}
