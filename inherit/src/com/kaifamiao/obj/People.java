package com.kaifamiao.obj;

public class People {

	public static void main(String[] args){
		
	}

}

class A {
	public int m;

	public void method() {
	}

	void add() {

	}

	public abstract class Q {
		abstract void add();
	}

	interface E {
		void count();
	}

	public class W {

	}

	int getM() {
		return m;
	}

	int seeM() {
		return m;
	}

}

class Man extends A implements B {
	int m;

	public void method() throws RuntimeException{
	}

	{

	}

	class T extends Q {

		@Override
		void add() {
			// TODO 自动生成的方法存根

		}

	}

	int getM() {
		return this.m;
	}

	public void add() {

	}

	@Override
	public void add(int a, int b) {
		// TODO 自动生成的方法存根

	}
}

interface B {
	static int a = 10;

	void add(int a, int b);

	static void add2() {

	}

	class X {
	}

	interface Y {
	}

	default void add1() {
		add5();

	}

	private void add5() {
	}

}

abstract class C {
	static {
	}

	public C() {

	}

	public abstract void add(double a, double b);

	interface Z {
	}
}
