package com.kaifamiao.obj;

public class Trapezoid extends Shape{
	private double top ;
	private double bottom;
	private double height;
	
	public Trapezoid( double top, double bottom, double height) {
		super("梯形");
		this.top = top;
		this.bottom = bottom;
		this.height = height;
	}

	public Trapezoid() {
		super("梯形");
	}
	public void calculate() {
		super.calculate();
		this.area=(top+bottom)*height/2;
	}
	public String toString() {
		String msg="上底为"+this.top+"下底为"+this.bottom+"高为"+this.height;
		return super.toString()+","+msg;
	}
}
