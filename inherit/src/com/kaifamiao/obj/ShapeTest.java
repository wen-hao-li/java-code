package com.kaifamiao.obj;

public class ShapeTest {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		Trapezoid t = new Trapezoid( 30 , 40 , 50 ); // 创建 Trapezoid 实例
		t.calculate() ; // 计算梯形面积
		System.out.println( t ); // 输出梯形信息和梯形面积

		Sector s = new Sector( 5,12 );
		s.calculate() ; // 计算扇形面积
		System.out.println( s ); // 输出扇形信息和扇形面积
		
		
	}

}
