package com.kaifamiao;

public class Triangle extends Shape{
	protected double firstEdge;
	protected double secondEdge;
	protected double thirdEdge;
	public Triangle(){
		this.type="三角形";
	}
	public void calculate() {
		double p=(firstEdge+secondEdge+thirdEdge)/2;
		this.area=Math.sqrt(p*(p-firstEdge)*(p-secondEdge)*(p-thirdEdge));
	}
	public void description() {
		System.out.println(this.type+"第一条边为"+firstEdge+"第二条边为"+secondEdge+"第三条边为"+thirdEdge);
		this.show();
	}
}
