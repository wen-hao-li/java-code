package com.kaifamiao;

public class ShapeTest {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		Triangle t = new Triangle(); // 创建 Triangle 实例
		t.firstEdge = 44 ; // 设置边长
		t.secondEdge = 49 ; // 设置边长
		t.thirdEdge = 52 ; // 设置边长
		t.calculate() ; // 计算三角形面积
		t.description(); // 输出三角形信息和三角形面积

		Circle c = new Circle();
		c.radius = 8 ; // 设置半径
		c.calculate() ; // 计算圆面积
		c.description(); // 输出圆半径和圆面积
	}

}
