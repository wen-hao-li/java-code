package com.liwenhao.test211116;

import java.util.Arrays;

public class Container {
	int count;
	Object[] elements;
	public Container() {
		super();
		elements = new Object[10];
	}
	public boolean add(Object o) {
		if(count!=0) {
			for(Object obj:elements) {
				if(o.equals(obj)) {
					System.out.println("添加失败");
					return false;
				}
			}
		}
		if(count==elements.length) {
			elements=Arrays.copyOf(elements, elements.length*2);
		}
		for(int i=0;i<elements.length;i++) {
			if(elements[i]==null) {
				elements[i]=o;
				count++;
				break;
			}
		}
		System.out.println("添加成功");
		return true;
	}
	public boolean remove(Object obj) {
		for(int i=0;i<elements.length;i++) {
			if(obj.equals(elements[i])) {
				elements[i]=null;
				count--;
				return true;
			}
		}
		return false;
	}
	public boolean contains(Object obj) {
		for(Object o:elements) {
			if(obj.equals(o)) {
				return true;
			}
		}
		return false;
	}
	public boolean set(int index,Object obj) {
		if(index<0||index>=elements.length) {
			System.out.println("下标超出范围");
			return false;
		}
		elements[index]=obj;
		return true;
	}
	public int size() {
		return count;
	}
	public boolean isEmpty() {
		return count==0;
	}
	public String toString() {
		String res="";
		for(Object obj:elements) {
			res+=obj+"\n";
		}
		return res;
	}
}
