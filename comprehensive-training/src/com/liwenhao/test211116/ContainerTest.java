package com.liwenhao.test211116;

public class ContainerTest {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		Container c = new Container();

		Employee e = new Employee( "DEV1001" , "岑参" , "男" , 22 );
		c.add( e ); // 返回 true
		System.out.println( c.size() ); // 1

		Employee m = new Employee( "DEV1001" , "岑参" , "男" , 22 );
		c.add( m ); // 返回 false
		
		System.out.println( c.size() ); // 1
		c.add(new Employee("DEV1002","美猴王","男",21));
		c.add(new Employee("DEV1003","齐天大圣","男",21));
		c.add(new Employee("DEV1004","白骨","男",21));
		c.add(new Employee("DEV1005","三藏","男",21));
		c.add(new Employee("DEV1006","悟能","男",21));
		c.add(new Employee("DEV1007","悟净","男",21));
		c.add(new Employee("DEV1008","悟空","男",21));
		System.out.println( c.size() ); // 8
		c.remove(new Employee("DEV1008","悟空","男",21));
		System.out.println( c.size() ); 
	}

}
