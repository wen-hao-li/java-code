package com.kaifamiao.test211123;

public class Node {
	private Node left;
	private char item;
	private Node right;
	public Node(char item) {
		super();
		this.item = item;
	}
	public Node getLeft() {
		return left;
	}
	public void setLeft(Node left) {
		this.left = left;
	}
	public char getItem() {
		return item;
	}
	public void setItem(char item) {
		this.item = item;
	}
	public Node getRight() {
		return right;
	}
	public void setRight(Node right) {
		this.right = right;
	}
	@Override
	public String toString() {
		if(left!=null&&right!=null) {
			return ""+left + item +  right;
		}
		if(left!=null) {
			return ""+left+item;
		}
		if(right!=null) {
			return ""+item+right;
		}
		return   ""+item ;
	}
	
	
}
