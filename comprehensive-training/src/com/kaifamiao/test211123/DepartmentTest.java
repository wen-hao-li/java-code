package com.kaifamiao.test211123;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;


public class DepartmentTest {
	public static void main(String[] args) {
		Department d=new Department();
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		try {
			Employee e1=new Employee(1001,"宋江",8000,"男",df.parse("1997-12-20"),df.parse("2013-2-15"));
			Employee e2=new Employee(1002,"武松",5000,"男",df.parse("1999-4-20"),df.parse("2017-2-24"));
			Employee e3=new Employee(1003,"吴用",7500,"男",df.parse("1998-5-20"),df.parse("2015-7-19"));
			Employee e4=new Employee(1004,"林冲",5500,"男",df.parse("1982-6-30"),df.parse("2013-5-24"));
			Employee e5=new Employee(1005,"鲁智深",4000,"男",df.parse("1989-8-19"),df.parse("2014-9-11"));
			Employee e6=new Employee(1006,"李逵",4500,"男",df.parse("1992-11-20"),df.parse("2016-12-05"));
			d.add(e1);
			d.add(e2);
			d.add(e3);
			d.add(e4);
			d.add(e5);
			d.add(e6);
		} catch (ParseException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		d.display();
		System.out.println("*".repeat(20));
		d.sortBySalary();
		d.display();
		System.out.println("*".repeat(20));
		d.sortByHiredate();
		d.display();
		System.out.println("*".repeat(20));
		d.showWorkingAge();
	}
}
