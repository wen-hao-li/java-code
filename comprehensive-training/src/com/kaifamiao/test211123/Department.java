package com.kaifamiao.test211123;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class Department {
	private Employee[] employees=new Employee[10];
	private int num=0;
	//添加员工 add(emp)
	public void add(Employee emp) {
		if(employees.length==num) {
			employees=Arrays.copyOf(employees, employees.length*2);
		}
		for(int i=0;i<employees.length;i++) {
			if(employees[i]==null) {
				employees[i]=emp;
				num++;
				return;
			}
		}
	}
	//按照薪水降序排序sortBySalary
	public void sortBySalary() {
		for(int i=0,len=employees.length;i<len-1;i++) {
			for(int j=0;j<len-1-i;j++) {
				if(employees[j]==null || employees[j+1]==null) {
					continue;
				}
				if(employees[j].getSalary()<employees[j+1].getSalary()) {
					Employee temp=employees[j];
					employees[j]=employees[j+1];
					employees[j+1]=temp;
				}
			}
		}
	}
	//按照入职日期升序排列sortByHiredate
	public void sortByHiredate() {
		for(int i=0,len=employees.length;i<len-1;i++) {
			for(int j=0;j<len-1-i;j++) {
				if(employees[j]==null || employees[j+1]==null) {
					continue;
				}
				if(employees[j].getBirthdate().after(employees[j+1].getBirthdate())) {
					Employee temp=employees[j];
					employees[j]=employees[j+1];
					employees[j+1]=temp;
				}
			}
		}
	}
	//输出员工的信息和工龄showWorkingAge
	public void showWorkingAge() {
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		
		System.out.println("id\t姓名\t性别\t工资\t生日\t\t入职时间\t\t工龄");
		for(Employee e:employees) {
			if(e!=null) {
				Calendar c=Calendar.getInstance();
				Calendar d=Calendar.getInstance();
				Date date=new Date();
				d.setTime(date);
				c.setTime(e.getHiredate());
				System.out.println(
						e.getId()+"\t"+
						e.getName()+"\t"+
						e.getGender()+"\t"+
						e.getSalary()+"\t"+
						df.format(e.getBirthdate())+"\t"+
						df.format(e.getHiredate())+"\t"+
						(d.get(Calendar.YEAR)-c.get(Calendar.YEAR))
						);
			}
		}
	}
	//显示所有员工信息 display
	public void display() {
		for(Employee e:employees) {
			if(e!=null) {
				System.out.println(e);
			}
		}
	}
}
