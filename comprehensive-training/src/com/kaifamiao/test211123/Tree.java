package com.kaifamiao.test211123;

public class Tree {
	private Node root;
	@SuppressWarnings("unused")
	private int count=0;

	public Tree(Node root) {
		this.root=root;
	}
	//添加节点add()
	public void add(char item) {
		Node node=new Node(item);
		treeAdd(root,node);
	}
	public void treeAdd(Node root ,Node node) {
		if(root.getItem()>=node.getItem()) {
			if(root.getLeft()!=null) {
				treeAdd(root.getLeft(),node);
				return;
			}
			root.setLeft(node);
			count++;
			return;
		}
		if(root.getItem()<node.getItem()) {
			if(root.getRight()!=null) {
				treeAdd(root.getRight(),node);
				return;
			}
			root.setRight(node);
			count++;
			return;
		}
		
	}
	//重写toString
	@Override
	public String toString() {
		return  ""+root ;
	}

}
