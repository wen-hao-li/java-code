package com.kaifamiao.test211125;

import java.util.LinkedList;
import java.util.List;

public class Test01 {

	public static void main(String[] args) {
		
		KfmLinkedList<String> kl=new KfmLinkedList<String>();
		System.out.println("链表是否空"+kl.isEmpty());
		System.out.println("链表长度"+kl.size());
		
		kl.add("曹操");
		kl.add("刘备");
		kl.add("孙权");
		System.out.println(kl);
		System.out.println("曹操是否在"+kl.contains("曹操"));
		System.out.println("链表是否空"+kl.isEmpty());
		System.out.println("链表长度"+kl.size());
		
		System.out.println("- - - ".repeat(15));
		
		kl.remove("曹操");
		System.out.println(kl);
		System.out.println("链表是否空"+kl.isEmpty());
		System.out.println("曹操是否在"+kl.contains("曹操"));
		System.out.println("链表长度"+kl.size());
		
		Object[] arr=kl.toArray();
		for(Object obj : arr) {
			System.out.println(obj);
		}

		System.out.println("- - - ".repeat(15));
		System.out.println("链表长度"+kl.size());
		LinkedList<String> list=new LinkedList<String>();
		list.add("赵云");
		list.add("吕布");
		list.add("关羽");
		kl.addAll(list);
		System.out.println(kl);
		System.out.println("链表长度"+kl.size());
		
		System.out.println("- - - ".repeat(15));
		List<String> list1=List.of("刘备","孙权","赵云","吕布");
		System.out.println(kl.containsAll(list1));
		
		System.out.println("- - - ".repeat(15));
		List<String> list2=List.of("赵云","吕布");
		kl.removeAll(list2);
		System.out.println(kl);
		System.out.println("链表长度"+kl.size());
		
		System.out.println("- - - ".repeat(15));
		List<String> list3=List.of("关羽");
		kl.retainAll(list3);
		System.out.println(kl);
		System.out.println("链表长度"+kl.size());
		
		kl.clear();
		System.out.println("-------清空");
		System.out.println(kl);
		System.out.println("链表长度"+kl.size());
		System.out.println("链表是否已经空"+kl.isEmpty());
	}

}
