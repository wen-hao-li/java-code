package com.kaifamiao.test211125;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class KfmLinkedList<T> implements Collection<T> {
	private class Node<F> {
		private F item;
		private Node<F> next;

		Node(F item) {
			this.setItem(item);
		}

		public void setNext(Node<F> e) {
			this.next = e;
		}

		public void setItem(F item) {
			this.item = item;
		}

		public F getItem() {
			return item;
		}

		public Node<F> getNext() {
			return next;
		}

		@Override
		public String toString() {
			return item + (next == null ? " " : "," + next);
		}

	}

	private Node<T> head = new Node<T>(null);
	private int count = 0;

	@Override
	public String toString() {
		return "[" + head.getNext() + "]";
	}

	@Override
	public int size() {
		return count;
	}

	@Override
	public boolean isEmpty() {
		return count == 0;
	}

	@Override
	public boolean contains(Object o) {
		Node<T> temp = head.next;
		while (temp != null) {
			if (o.equals(temp.getItem())) {
				return true;
			}
			temp = temp.getNext();
		}
		return false;
	}

	@Override
	public Iterator<T> iterator() {

		return null;
	}

	@Override
	public Object[] toArray() {
		Node<T> temp = head.next;
		Object[] objs = new Object[count];
		int n = 0;
		while (temp != null) {
			objs[n] = temp.getItem();
			n++;
			temp = temp.getNext();
		}
		return objs;
	}

	@SuppressWarnings("hiding")
	@Override
	public <T> T[] toArray(T[] a) {

		return null;
	}

	@Override
	public boolean add(T e) {
		if(e==null) {
			throw new IllegalArgumentException("空的不允许添加单项链表集合");
		}
		Node<T> node = new Node<T>(e);
		if (head.next == null) {
			head.setNext(node);
			count++;
			return true;
		}
		Node<T> temp = head.next;
		while (temp.next != null) {
			temp = temp.next;
		}
		temp.next = node;
		count++;
		return true;
	}

	@Override
	public boolean remove(Object o) {
		Node<T> temp = head;
		while (temp.next != null) {
			if (temp.getNext().getItem().equals(o)) {
				temp.setNext(temp.getNext().getNext());
				count--;
				return true;
			}
			temp = temp.getNext();
		}
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		if(c==null) {
			throw new IllegalArgumentException("空的不存在");
		}
		if(c.size()>count) {
			return false;
		}
		Object[] arr=c.toArray();
		for(int i=0;i<arr.length;i++) {
			if(!this.contains(arr[i])) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		for (T obj : c) {
			add(obj);
		}
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		if(c==null) {
			throw new IllegalArgumentException("空的不存在");
		}
		Object[] arr=c.toArray();
		for(int i=0;i<arr.length;i++) {
			this.remove(arr[i]);
		}
		return true;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		if(c==null) {
			throw new IllegalArgumentException("空的不存在");
		}
		Object[] arr=c.toArray();
		count=0;
		LinkedList<T> kfm=new LinkedList<T>();
		for(int i=0;i<arr.length;i++) {
			Node<T> temp=head.next;
			while(temp!=null) {
				if(temp.getItem().equals(arr[i])) {
					kfm.add(temp.getItem());
					temp=temp.getNext();
					continue;
				}
				temp=temp.getNext();
			}
		}
		this.clear();
		this.addAll(kfm);
		return true;
	}

	@Override
	public void clear() {
		head.setNext(null);
		count = 0;
	}

}
