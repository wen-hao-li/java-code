package com.kaifamiao.test211122;

public class DeptTest {

	public static void main(String[] args) {
		Department d = new Department();
		try {
			Employee e = new Employee(1001, "雷军", "男", 8000);
			d.add(e);
			Employee m = new Employee(1002, "周鸿祎", "男", 8500);
			d.add(m);
			Employee p = new Employee(1003, "马云", "男", 5500);
			d.add(p);
			Employee y = new Employee(1004, "李彦宏", "男", 6500);
			d.add(y);
			d.display();
			System.out.println("***************");
			d.sort();
			d.display();
			System.out.println("***************");
			d.delete(e);
			d.display();
			System.out.println("***************");
			System.out.println(d.selectById(1003));
			System.out.println("***************");
			d.add(e);
			d.update(1004);
			d.sort();
			d.display();
		} catch (ExtendException e) {
			System.err.println(e.getMessage());
		}
	}

}
