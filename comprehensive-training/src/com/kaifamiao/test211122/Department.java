package com.kaifamiao.test211122;

import java.util.Arrays;
import java.util.Scanner;

public class Department {
	private Employee[] employess = new Employee[10];
	private int count = 0;

	// 添加
	public void add(Employee emp) throws ExtendException {
		if (employess.length == count) {
			employess = Arrays.copyOf(employess, employess.length * 2);
		}
		int temp = 0;
		for (int i = 0; i < employess.length; i++) {
			if (employess[i] != null) {
				if (employess[i].equals(emp)) {
					throw new ExtendException("添加的员工已存在");
				}
				continue;
			}
			if (temp == 0) {
				temp = i;
			}
		}
		employess[temp] = emp;
		count++;
	}

	// 删除
	public void delete(Employee emp) throws ExtendException {
		boolean flag = true;
		int temp = 0;
		for (int i = 0; i < employess.length; i++) {
			if (employess[i] != null) {
				if (employess[i].equals(emp)) {
					temp = i;
					flag = false;
					break;
				}
			}
		}
		if (flag) {
			throw new ExtendException("删除的员工不存在");
		}
		employess[temp] = null;
		count--;
	}

	// 查找
	public Employee selectById(int id) throws ExtendException {
		boolean flag = true;
		Employee temp = null;
		for (Employee e : employess) {
			if (e != null) {
				if (e.getId() == id) {
					temp = e;
					flag = false;
					break;
				}
			}
		}
		if (flag) {
			throw new ExtendException("查找的员工不存在");
		}
		return temp;
	}

	// 修改
	public void update(int id) throws ExtendException {
		boolean flag = true;
		Employee temp = null;
		for (Employee e : employess) {
			if (e != null) {
				if (e.getId() == id) {
					temp = e;
					flag = false;
					break;
				}
			}
		}
		if (flag) {
			throw new ExtendException("修改的员工不存在");
		}
		Scanner s = new Scanner(System.in);
		System.out.println("输入修改的工资");
		double sal = s.nextDouble();
		temp.setSalary(sal);
		s.close();
	}

	// 按照salary降序排序 sort方法
	public void sort() {
		for (int i = 0; i < employess.length - 1; i++) {
			for (int j = 0; j < employess.length - 1 - i; j++) {
				if (employess[j] != null && employess[j + 1] != null) {
					if (employess[j].compareTo(employess[j + 1]) > 0) {
						Employee temp = employess[j];
						employess[j] = employess[j + 1];
						employess[j + 1] = temp;
					}
				}

			}
		}
	}

	// 输出员工信息 display方法
	public void display() {
		for (Employee emp : employess) {
			if (emp != null) {
				System.out.println(emp);
			}

		}
	}
}
