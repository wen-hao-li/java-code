package com.kaifamiao.test211122;

public class ExtendException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	public ExtendException() {
		super();
		// TODO 自动生成的构造函数存根
	}

	public ExtendException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO 自动生成的构造函数存根
	}

	public ExtendException(String message, Throwable cause) {
		super(message, cause);
		// TODO 自动生成的构造函数存根
	}

	public ExtendException(String message) {
		super(message);
		// TODO 自动生成的构造函数存根
	}

	public ExtendException(Throwable cause) {
		super(cause);
		// TODO 自动生成的构造函数存根
	}

}
