package com.kaifamiao.test211117;

public class TowerOfHanoi {
	 public void move(int n, char a, char b, char c) {
		 if(n==1){
				System.out.println("move from "+a+" to "+c);
			}else{
				move(n-1, a, c, b);
				move(  1, a, b, c);
				move(n-1, b, a, c);
			}
   }
	public static void main(String[] args) {
		
		 TowerOfHanoi ht=new TowerOfHanoi();
         ht.move(6,'A','B','C');
	}
}
