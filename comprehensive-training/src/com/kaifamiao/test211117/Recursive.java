package com.kaifamiao.test211117;


public class Recursive {
	public String reverse(String str) {
		if(str.length()<=1) {
			return str;
		}
		return reverse(str.substring(1))+str.charAt(0);
	}
	public int reverse(int num) {
		
		if(num<10) {
			return num;
		}
		int x=(int)Math.pow(10, String.valueOf(num).length()-1);
		return	num/x+10*reverse(num%x);
	}
	public int mi(int x,int n) {
		if(n<0) {
			System.out.println("分数还没有实现");
			return -1;
		}
		if(n==0) {
			return 1;
		}
		if(n==1) {
			return x;
		}
		return x*mi(x,n-1);
	}
	public static void main(String[] args) {
		Recursive r=new Recursive();
		System.out.println(r.reverse("abcdefg"));
		System.out.println(r.reverse(1989));
		System.out.println(r.mi(2, 12));
	}
}
