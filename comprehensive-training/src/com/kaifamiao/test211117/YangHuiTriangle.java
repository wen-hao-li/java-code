package com.kaifamiao.test211117;
public class YangHuiTriangle {
	public static void main(String[] args) {
		YangHuiTriangle t=new YangHuiTriangle();
		t.printT(13);
	}
	public void printT(int n) {
		int[][] arr=new int[n][n];
		for(int i=0;i<n;i++) {
			arr[i][0]=1;
			arr[i][i]=1;
			if(i>=2)for(int j=1;j<i;j++)arr[i][j]=arr[i-1][j-1]+arr[i-1][j];
		}
		for(int i=0 ;i<n;i++) {
			for(int k=0;k<n-i-1;k++)System.out.print("  ");
			for(int j=0;j<=i;j++)System.out.printf("%3d ",arr[i][j]);
			System.out.println();
		}
	}
}