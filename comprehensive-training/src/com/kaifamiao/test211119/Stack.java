package com.kaifamiao.test211119;

public class Stack {
	private Node head=new Node(null,null);;
	public int size() {
		int count=0;
		if(head.getNext()==null) {
			return count;
		}
		Node temp=head.getNext();
		while(temp.getNext()==null) {
			count++;
			temp=temp.getNext();
		}
		return count;
	}
	public void push(Node item) {
		if(head.getNext()==null) {
			head.setNext(item);
			return;
		}
		item.setNext(head.getNext());
		head.setNext(item);
	}
	public Node peek() {
		if(head.getNext()==null) {
			return null;
		}
		return head.getNext();
	}
	public Node pop() {
		if(head.getNext()==null) {
			return null;
		}
		Node temp=head.getNext();
		head.setNext(head.getNext().getNext());
		return temp;
	}
	public void show() {
		if(head.getNext()==null) {
			System.out.println("栈为空");
			return ;
		}
		Node temp=head.getNext();
		while(temp!=null) {
			System.out.println(temp.toString());
			temp=temp.getNext();
		}
	}
}
