package com.kaifamiao.test211119;

import java.util.Calendar;

public class Test02 {
	public void hello() {
		Calendar c=Calendar.getInstance();
		int clock=c.get(Calendar.HOUR_OF_DAY);
		int min=c.get(Calendar.MINUTE);
		int sec=c.get(Calendar.SECOND);
		System.out.println("现在时间是"+clock+"时"+min+"分"+sec+"秒");
		if(clock<=6) {
			System.out.println("凌晨好");
			return;
		}
		if(clock<=8) {
			System.out.println("上午好");
			return;
		}
		if(clock<=12) {
			System.out.println("中午好");
			return;
		}
		if(clock<=18) {
			System.out.println("下午好");
			return;
		}
		if(clock<24) {
			System.out.println("晚上好");
			return;
		}
	}
	public static void main(String[] args) throws InterruptedException {
		Test02 t=new Test02();
		while(true) {
			t.hello();
			Thread.sleep(10000);
			
		}
	}
}
