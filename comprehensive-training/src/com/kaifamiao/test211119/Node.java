package com.kaifamiao.test211119;

public class Node {
	private Phoenix item;
	private Node next;
	public Node(Phoenix item, Node next) {
		super();
		this.item = item;
		this.next = next;
	}
	public Phoenix getItem() {
		return item;
	}
	public void setItem(Phoenix item) {
		this.item = item;
	}
	public Node getNext() {
		return next;
	}
	public void setNext(Node next) {
		this.next = next;
	}
	@Override
	public String toString() {
		return "Node [item=" + item + "]";
	}
}
                                              