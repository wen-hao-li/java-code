package com.kaifamiao.test211119;

public class Phoenix implements Comparable<Phoenix>{
	private String name;
	private int age;
	private char gender;
	public static String name1 ;
	public Phoenix(String name, int age, char gender) {
		super();
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "Phoenix {name:'" + name + "', age:" + age + ", gender'" + gender + "'}";
	}
	@Override
	public int compareTo(Phoenix o) {
		// TODO 自动生成的方法存根
		return this.age-o.age;
	}
	
}
