package com.kaifamiao.test211118;

import java.util.Arrays;
import java.util.Comparator;

public class PhoenixTest {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		Phoenix[] phoenixs=new Phoenix[5];
		phoenixs[ 0 ] = new Phoenix( "凤凰卫士" , 150 , '男' );
		phoenixs[ 1 ] = new Phoenix( "凤凰大侠" , 200 , '男' );
		phoenixs[ 2 ] = new Phoenix( "凤凰传奇" , 500 , '女' );
		phoenixs[ 3 ] = new Phoenix( "凤凰恶魔" , 250 , '男' );
		phoenixs[ 4 ] = new Phoenix( "凤凰天使" , 100 , '女' );
		for(Phoenix p:phoenixs) {
			System.out.println(p);
		}
		Arrays.sort(phoenixs);
		System.out.println("*************");
		for(Phoenix p:phoenixs) {
			System.out.println(p);
		}
		System.out.println("*************");
		Comparator<Phoenix> comparator=new Comparator<Phoenix>() {
			@Override
			public int compare(Phoenix o1, Phoenix o2) {
				if(o1.getGender()=='女'&& o2.getGender()=='男') {
					return -1;
				}
				if(o1.getGender()==o2.getGender()){
					return 0;
				}
				if(o1.getGender()=='男'&& o2.getGender()=='女') {
					return 1;
				}
				return Integer.MIN_VALUE;
			}
		};
		Arrays.sort(phoenixs, comparator);
		for(Phoenix p:phoenixs) {
			System.out.println(p);
		}
	}

}
