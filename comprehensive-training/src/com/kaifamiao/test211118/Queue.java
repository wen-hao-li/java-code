package com.kaifamiao.test211118;

public class Queue {
	private Node head=new Node(null,null);
	//size方法返回链表节点个数
	public int size() {
		int count=0;
		if(head.getNext()==null) {
			return count;
		}
		Node temp=head.getNext();
		while(temp!=null) {
			count++;
			temp=temp.getNext();
		}
		return count;
	}
	//add添加节点到链表末尾
	public void add(Node node) {
		Node temp=head;
		while(temp.getNext()!=null) {
			temp=temp.getNext();
		}
		temp.setNext(node);
	}
	//peek方法检测队列头部元素
	public Node peek() {
		if(head.getNext()==null) {
			return null;
		}
		return head.getNext();
	}
	//poll方法获取并删除队列头部元素
	public Node poll() {
		if(head.getNext()==null) {
			return null;
		}
		Node temp=head.getNext();
		head.setNext(head.getNext().getNext());
		return temp;
	}
	//show方法来遍历查看链表
	public void show() {
		if(head.getNext()==null) {
			System.out.println("链表为空");
			return ;
		}
		Node temp=head.getNext();
		while(temp!=null) {
			System.out.println(temp.toString());
			temp=temp.getNext();
		}
	}
}
