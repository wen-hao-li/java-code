package com.kaifamiao.test211118;

public class Test01 {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		Phoenix p1= new Phoenix( "凤凰卫士" , 150 , '男' );
		Phoenix p2= new Phoenix( "凤凰大侠" , 200 , '男' );
		Phoenix p3= new Phoenix( "凤凰传奇" , 500 , '女' );
		Phoenix p4= new Phoenix( "凤凰恶魔" , 250 , '男' );
		Phoenix p5= new Phoenix( "凤凰天使" , 100 , '女' );
		Node n1=new Node(p1,null);
		Node n2=new Node(p2,null);
		Node n3=new Node(p3,null);
		Node n4=new Node(p4,null);
		Node n5=new Node(p5,null);
		Queue q=new Queue();
		q.add(n1);
		q.show();
		System.out.println("*****************");
		q.add(n3);
		q.add(n4);
		q.add(n2);
		q.add(n5);
		q.show();
		System.out.println("*****************");
		System.out.println(q.peek());
		System.out.println("*****************");
		q.show();
		System.out.println("*****************");
		System.out.println(q.poll());
		System.out.println("*****************");
		q.show();
	}

}
