package com.kaifamiao.test211126;

public class ExistException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4181623713773800980L;

	public ExistException() {
		super();
		// TODO 自动生成的构造函数存根
	}

	public ExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO 自动生成的构造函数存根
	}

	public ExistException(String message, Throwable cause) {
		super(message, cause);
		// TODO 自动生成的构造函数存根
	}

	public ExistException(String message) {
		super(message);
		// TODO 自动生成的构造函数存根
	}

	public ExistException(Throwable cause) {
		super(cause);
		// TODO 自动生成的构造函数存根
	}
	
}
