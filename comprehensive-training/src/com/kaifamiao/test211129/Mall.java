package com.kaifamiao.test211129;

import java.util.ArrayList;
import java.util.Scanner;


public class Mall {
	public static ArrayList<Product> pList = new ArrayList<>();
	static Scanner s=new Scanner(System.in);
	public static void main(String[] args) {
		pList.add(new Product(1, "毛巾", 5, 8.5, "$"));
		pList.add(new Product(2, "牙刷", 2, 10, "￥"));
		pList.add(new Product(3, "牙膏", 10, 8.5, "$"));
		pList.add(new Product(4, "拖鞋", 15, 9, "$"));
		pList.add(new Product(5, "洗面奶", 58, 9, "$"));
		pList.add(new Product(6, "沐浴露", 55, 7.9, "$"));
		pList.add(new Product(7, "电吹风", 68, 9, "$"));
		System.out.println("欢迎使用购物系统");
		ShoppingCar sc=new ShoppingCar();
		boolean flag=true;
		while(flag) {
			System.out.println("- - - ".repeat(7));
			System.out.println("选择购物车操作");
			System.out.println("01.添加商品");
			System.out.println("02.查看购物车");
			System.out.println("03.删除购物车中商品");
			System.out.println("04.查看回收站");
			System.out.println("05.恢复某商品");
			System.out.println("06.清空购物车");
			System.out.println("07.退出");
			System.out.println("输入操作数");
			int key = s.nextInt();
			switch(key){
			case 1:
				showGoods();
				System.out.println("选择添加商品的商品编号");
				int id=s.nextInt();
				System.out.println("选择数量");
				int count=s.nextInt();
				sc.add(id, count);
				break;
			case 2:
				sc.show();
				break;
			case 3:
				System.out.println("选择删除商品的商品名");
				String rename=s.next();
				sc.remove(rename);
				break;
			case 4:
				sc.showRe();
				break;
			case 5:
				System.out.println("选择恢复商品的商品名");
				String goodsname=s.next();
				sc.reGoods(goodsname);
				break;
			case 6:
				sc.clean();
				break;
			case 7:
				s.close();
				flag=false;
				break;
			default:
				System.out.println("输入有误重写输入");
				break;
			}
		}
		System.out.println("谢谢使用，再见");
		System.exit(0);
	}
	private static void showGoods() {
		System.out.println("商品列表:");
		for(Product p:pList) {
			System.out.println(p);
		}
	}

	
}
