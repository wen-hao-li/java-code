package com.kaifamiao.test211129;

import java.util.HashMap;
import java.util.Map;

/**
 * 根据用户输入的商品id和数量添加商品 add 
 * 查询购物车(显示每种商品及其数量，总价) show
 * 删除某一商品(从购物车到回收站) remove
 * 查看回收站(即删除的商品) showRe
 * 恢复某一商品(从回收站到购物车) reGoods
 * 清空购物车(回收站和购物车都清空) clean
 * 
 * @author lenao
 *
 */
public class ShoppingCar {
	private Map<Product, Integer> list = new HashMap<>();
	private Map<Product, Integer> listRe = new HashMap<>();
	public boolean add(int id, int count) {
		try {
			for(Map.Entry<Product, Integer> en:list.entrySet()) {
				if(en.getKey().getId()==id) {
					list.put(en.getKey(), en.getValue()+count);
					System.out.println("添加成功");
					return true;
				}
			}
			list.put(Mall.pList.get(id - 1), count);
		} catch (RuntimeException e) {
			System.err.println("添加失败");
			return false;
		}
		System.out.println("添加成功");
		return true;
	}

	public void show() {
		System.out.printf("%-4s\t%-4s\t%-4s\n", "商品名", "数量", "总价");
		for (Map.Entry<Product, Integer> en : list.entrySet()) {
			System.out.printf("%-4s\t%-4d\t%-4.3f%s\n", en.getKey().getName(), en.getValue(),
					en.getKey().getPrice() * en.getValue() * en.getKey().getDiscount()/10,en.getKey().getUnits());
		}
	}

	public boolean remove(String name) {
		for(Map.Entry<Product, Integer> en: list.entrySet()) {
			if(en.getKey().getName().equals(name)) {
				for(Map.Entry<Product, Integer> enr:listRe.entrySet()) {
					if(enr.getKey().getName().equals(name)) {
						listRe.put(enr.getKey(),en.getValue()+enr.getValue());
						list.remove(Mall.pList.get(en.getKey().getId()-1));
						System.out.println("删除成功");
						return true;
					}
				}
				listRe.put(Mall.pList.get(en.getKey().getId()-1),list.get(Mall.pList.get(en.getKey().getId()-1)));
				list.remove(Mall.pList.get(en.getKey().getId()-1));
				System.out.println("删除成功");
				return true;
			}
		}
		System.out.println("没有找到待删除的商品");
		return false;
	}
	
	public void showRe() {
		System.out.println("回收站");
		System.out.printf("%-4s\t%-4s\t%-4s\n", "商品名", "数量", "总价");
		for(Map.Entry<Product, Integer> en:listRe.entrySet()) {
			System.out.printf("%-4s\t%-4d\t%-4.1f%s\n", en.getKey().getName(), en.getValue(),
					en.getKey().getPrice() * en.getValue() * en.getKey().getDiscount()/10,en.getKey().getUnits());
		}
	}
	
	public boolean reGoods(String name){
		for(Map.Entry<Product, Integer> en:listRe.entrySet()) {
			if(en.getKey().getName().equals(name)) {
				for(Map.Entry<Product, Integer> enl:list.entrySet()) {
					if(enl.getKey().getName().equals(name)) {
						list.put(en.getKey(), en.getValue()+enl.getValue());
						listRe.remove(en.getKey());
						System.out.println("商品恢复成功");
						return true;
					}
				}
				list.put(en.getKey(), en.getValue());
				listRe.remove(en.getKey());
				System.out.println("商品恢复成功");
				return true;
			}
		}
		System.out.println("未找到要恢复的商品");
		return false;
		
	}
	
	public boolean clean() {
		for(Map.Entry<Product, Integer> enl:list.entrySet()) {
			for(Map.Entry<Product, Integer> en:listRe.entrySet()) {
				if(en.getKey().getName().equals(enl.getKey().getName())) {
					listRe.put(en.getKey(),en.getValue()+enl.getValue());
					list.remove(enl.getKey());
					break;
				}
			}
		}
		listRe.putAll(list);
		list.clear();
		System.out.println("已清空购物出");
		return true;
	}
}