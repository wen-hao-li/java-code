package com.kaifamiao.inface;

public abstract class Machine{
	private int energy ;

	public Machine(int energy) {
		super();
		this.energy = energy;
	}

	public int getEnergy() {
		return energy;
	}

	public void setEnergy(int energy) {
		this.energy = energy;
	}
	
}
