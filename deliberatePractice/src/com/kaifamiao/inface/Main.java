package com.kaifamiao.inface;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		Random rand=new Random();
		Machine m=new Aircraft();
		while(true) {
			int op=rand.nextInt(10);
			switch(op) {
			case 0:
				if(m.getEnergy()>=10) {
					Roadable m1=(Roadable)m;
					m1.run();
					continue;
				}
				break;
			case 1:
				if(m.getEnergy()>=15) {
					Voyageable m1=(Voyageable)m;
					m1.voyage();;
					continue;
				}
				break;
			case 2:
				if(m.getEnergy()>=25) {
					Flyable m1=(Flyable)m;
					m1.fly();
					continue;
				}
				break;
			default:
				continue;
			}
			System.out.println("飞行器剩余"+m.getEnergy());
			return;
		}
		
	}

}
