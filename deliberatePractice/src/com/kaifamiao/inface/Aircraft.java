package com.kaifamiao.inface;

public class Aircraft extends Machine implements Voyageable,Roadable,Flyable{
	
	public Aircraft() {
		super(100);

	}

	@Override
	public void voyage() {
		this.setEnergy(this.getEnergy()-15);
	}

	@Override
	public void run() {
		this.setEnergy(this.getEnergy()-10);
		
	}

	@Override
	public void fly() {
		this.setEnergy(this.getEnergy()-25);
		
	}

}
