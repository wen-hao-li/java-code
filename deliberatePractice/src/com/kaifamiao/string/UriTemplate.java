package com.kaifamiao.string;

import java.util.HashMap;
import java.util.Map;

public class UriTemplate {
	private String template;
	
	public UriTemplate() {
		super();
		this.template = "/student/:id/:name";
	}

	public Map<String,String> parse(String uri){
		Map<String,String> map = new HashMap<String,String>();
//		String tempS=this.template.replace(":", "");
		String[] temp=template.split("/:");
		String[] uriStr=uri.split("/");
		for(int i=1;i<temp.length;i++) {
				map.put(temp[i], uriStr[i+1]);
		}
		return map;
	}
	public void show() {
		System.out.println(parse("/student/1001/卫青"));
	}
	public static void main(String[] args) {
		UriTemplate u=new UriTemplate();
		u.show();
	}
}