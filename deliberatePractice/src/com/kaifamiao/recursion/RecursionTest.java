package com.kaifamiao.recursion;

public class RecursionTest {
	public static void main(String[] args) {
		RecursionDemo rD=new RecursionDemo();
		
		System.out.println(rD.sum(5));//15
		System.out.println(rD.odd(5));//1+3+5=9
		System.out.println(rD.even(5));//2+4=6
		System.out.println(rD.factorial(5));//5*4*3*2*1=120
		System.out.println(rD.fibonacci(5));//1 1 2 3 5
		System.out.println(rD.greatest(30, 45));// 15
		System.out.println(rD.count("Hello Wrold", 'l'));//3
		System.out.println(rD.count("Hello Wrold", 'e'));//1
		System.out.println(rD.sumDigit(50));//6
	}
}
