package com.kaifamiao.recursion;

public class RecursionDemo {
	static int count =0;
	//求前N项和
	public int sum(int n) {
		if(n<0) {
			return -1;
		}
		if(n==0) {
			return 0;
		}
		return n+sum(n-1);
	}
	//求前N项奇数和
	public int odd(int n) {
		if(n<0) {
			return -1;
		}
		if(n==0) {
			return 0;
		}
		if(n%2!=0) {
			return n+odd(n-1);
		}
		return odd(n-1);
	}
	//求前N项偶数和
	public int even(int n) {
		if(n<0) {
			return -1;
		}
		if(n==0) {
			return 0;
		}
		if(n%2==0) {
			return n+even(n-1);
		}
		return even(n-1);
	}
	//n的阶乘
	public long factorial(long n) {
		if(n<0) {
			return -1;
		}
		if(n==0 || n==1) {
			return 1;
		}
		return n*factorial(n-1);
	}
	//斐波那契数的第N项
	public int fibonacci(int n) {
		if(n<0) {
			return -1;
		}
		if(n==0) {
			return 0;
		}
		if(n==1) {
			return 1;
		}
		return fibonacci(n-1)+fibonacci(n-2);
	}
	//求两数最大公约数
	public int greatest(int m,int n) {
		if(m<=0||n<=0) {
			System.out.print("请输入正确的正整数");
			return -1;
		}
		if(m>n) {
			int y=m%n;
			if(y==0) {
				return n;
			}
			return greatest(n,y);
		}
		int y=n%m;
		if(y==0) {
			return m;
		}
		return greatest(m,y);
	}
	//统计指定字符c在字符串中出现的次数
	public int count(String s,char c) {
		if(s.indexOf(c)<0) {
			int n=count;
			count=0;
			return n;
		}
		String s1=s.replaceFirst(""+c,"");
		count++;
		return count(s1,c);
	}
	//计算各个位数的和
	public int sumDigit(long n) {
		if(n<0) {
			System.out.print("输入正确的正整数");
			return -1;
		}
		if(n==0) {
			int res=count;
			count=0;
			return res;
		}
		count+=n%10;
		return sumDigit(n/10);
	}
}
