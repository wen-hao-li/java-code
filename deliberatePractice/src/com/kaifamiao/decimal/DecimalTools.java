package com.kaifamiao.decimal;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class DecimalTools {

	public static String add(String s1, String s2) {
		BigDecimal d1=new BigDecimal(s1);
		BigDecimal d2=new BigDecimal(s2);
		return d1.add(d2).toString();
	}

	public static String divide(String s1, String s2) {
		BigDecimal d1=new BigDecimal(s1);
		BigDecimal d2=new BigDecimal(s2);
		MathContext mc=new MathContext(2,RoundingMode.HALF_UP);
		return d1.divide(d2,mc).toString();
	}

	public static String multiply(String s1, String s2) {
		BigDecimal d1=new BigDecimal(s1);
		BigDecimal d2=new BigDecimal(s2);
		return d1.multiply(d2).toString();
	}

	public static String subtract(String s1, String s2) {
		BigDecimal d1=new BigDecimal(s1);
		BigDecimal d2=new BigDecimal(s2);
		return d1.subtract(d2).toString();
	}
	public static void main(String[] args) {
		System.out.println(add("10.56","1.44"));
		System.out.println(divide("10.56","1.44"));
		System.out.println(multiply("10.56","1.44"));
		System.out.println(subtract("10.56","1.44"));
	}
}
