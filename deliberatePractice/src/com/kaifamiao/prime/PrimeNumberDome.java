package com.kaifamiao.prime;

public class PrimeNumberDome {
	//判断是否是素数
	public boolean isPrimeNum(int n) {
		if(n<2) {
			return false;
		}
		for(int i=2;i<=n/2;i++) {
			if(n%i==0) {
				return false;
			}
		}
		return true;
	}
	//求100以内的素数
	public void showPrimeNum(int n) {
		for(int i=2;i<=n;i++) {
			if(isPrimeNum(i)) {
				System.out.print(i+"\t");
			}
		}
	}
	//求从2开始统计的100个素数
	public void showCountPrimeNum(int n) {
		if(n<=0) {
			return;
		}
		int count=0;
		int i=2;
		while(true) {
			if(isPrimeNum(i)) {
				if(count++==n) {
					break;
				}
				System.out.print("第"+count+"个素数"+i++);
				System.out.print("\t");
				continue;
			}	
			i++;
		}
	}
	//求1000以内的所有孪生素数
	public void showTwinPrimeNum(int n) {
		if(n<=4) {
			return;
		}
		for(int i=2;i<=n;i++) {
			if( isPrimeNum(i) && isPrimeNum(i+2)) {
				System.out.println(+i+"和"+(i+2)+"为孪生素数");
			}
		}
	}
	//求100对孪生素数
	public void showCountTwinPrimeNum(int n) {
		if(n<=0) {
			return;
		}
		int count=0;
		int i=3;
		while(true) {
			if(isPrimeNum(i) && isPrimeNum(i+2)) {
				if(count++==n) {
					break;
				}
				System.out.println("第"+count+"对孪生素数为"+i+"和"+(i+2));
				i++;
			}
			i++;
		}
	}
}
