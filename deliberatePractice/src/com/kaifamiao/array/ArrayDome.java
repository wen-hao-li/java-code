package com.kaifamiao.array;

public class ArrayDome {
	public void traverse(int[] arr) {
		for (int n : arr) {
			System.out.print(n+"\t");
		}
	}
	public void bubble(int[] arr) {
		for(int i=0;i<arr.length-1;i++) {
			for(int j=0;j<arr.length-i-1;j++) {
				if(arr[j]>arr[j+1]) {
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
	}
	public void quick(int[] arr,int first,int last) {
		if(first>last) {
			return;
		}
		int left=first;
		int right=last;
		int temp=arr[first];
		int t;
		while(left<right) {
			while(left<right&& arr[right]>=temp) {
				right--;
			}
			while(left<right && arr[left]<=temp) {
				left++;
			}
			if(left<right) {
				t=arr[left];
				arr[left]=arr[right];
				arr[right]=t;
			}
		}
		arr[first]=arr[left];
		arr[left]=temp;
		quick(arr,left+1,last);
		quick(arr,first,left-1);
	}
	public void insert(int[] arr) {
		for(int i=1;i<arr.length;i++) {
			for(int j=i;j>0;j--) {
				if(arr[j]>arr[j-1]) {
					int temp=arr[j];
					arr[j]=arr[j-1];
					arr[j-1]=temp;
					continue;
				}
				break;
			}
		}
	}
	
	
}
