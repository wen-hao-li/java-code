package com.kaifamiao.array;

public class PhoneixTest {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		Phoenix[] phoenixs = new Phoenix[ 5 ];
		phoenixs[ 0 ] = new Phoenix( "凤凰卫士" , 150 , '男' );
		phoenixs[ 1 ] = new Phoenix( "凤凰大侠" , 200 , '男' );
		phoenixs[ 2 ] = new Phoenix( "凤凰传奇" , 250 , '女' );
		phoenixs[ 3 ] = new Phoenix( "凤凰奇传" , 300 , '男' );
		phoenixs[ 4 ] = new Phoenix( "凤凰天使" , 100 , '女' );
		PhoneixTest pt=new PhoneixTest();
		pt.bubble(phoenixs);
		pt.show(phoenixs);
	}
	public void bubble(Phoenix[] arr) {
		for(int i=0;i<arr.length-1;i++) {
			for(int j=0;j<arr.length-1-i;j++) {
				if(arr[j].getAge()>arr[j+1].getAge()) {
					Phoenix temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
	}
	public void show(Phoenix[] arr) {
		for(Phoenix p:arr) {
			System.out.println(p);
		}
	}
}
