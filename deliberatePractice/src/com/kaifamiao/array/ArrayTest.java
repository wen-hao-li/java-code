package com.kaifamiao.array;

public class ArrayTest {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		ArrayDome ad = new ArrayDome();
		int[] a = { 5,1, 2, 3, 4 };
		int[] b = {5,1,2,3,4,12,312,12341,12,541,6,5,1,-1,0,12};
		int[] c = {5,1,1,3,4,12,312,12341,12,541,6,5,1,-1,0,12};
		ad.traverse(a);
		ad.bubble(a);
		ad.traverse(a);
		System.out.println();
		ad.quick(b, 0, b.length-1);
		ad.traverse(b);
		System.out.println();
		ad.insert(c);
		ad.traverse(c);
	}
}
