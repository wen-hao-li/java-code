package com.kaifamiao.datetime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

public class DateTimeTest01 {
	static final String TIANGAN = "甲乙丙丁戊己庚辛壬癸";
	static final String DIZHI = "子丑寅卯辰巳午未申酉戌亥";
	static final String ANIMAL = "鼠牛虎兔龙蛇马羊猴鸡狗猪";

	public static void main(String[] args) {
		Date date1 = new Date();
		Date date2 = new Date(1000L * 60 * 60 * 24 * 365 * 30);
		DateTimeTest01 dtt = new DateTimeTest01();

		System.out.println("Calendar计算相差天");
		System.out.println(dtt.calenderDayOf(date1, date2));
		System.out.println("datetime计算相差天");
		System.out.println(dtt.datetimeDayOf(date1, date2));

		System.out.println("Calendar计算相差秒");
		System.out.println(dtt.calenderSecondOf(date1, date2));
		System.out.println("datetime计算相差秒");
		System.out.println(dtt.datetimeSecondOf(date1, date2));

		System.out.println("今年对应的干支纪年以及生肖：");
		System.out.println(dtt.zodiacSign(date1));
		System.out.println("1999对应的干支纪年以及生肖：");
		System.out.println(dtt.zodiacSign(date2));
	}

	// date转calendar
	private Calendar typeToCalendar(Date d) {
		Calendar c = Calendar.getInstance();

		c.setTime(d);

		return c;
	}

	// date转localdate
	private LocalDate typeToDate(Date d) {
		Instant ins = d.toInstant();

		ZoneId zid = ZoneId.of("Asia/Shanghai");
		ZonedDateTime zdt = ins.atZone(zid);

		LocalDate ldt1 = zdt.toLocalDate();

		return ldt1;
	}

	private LocalTime typeToTime(Date d) {
		Instant ins = d.toInstant();

		ZoneId zid = ZoneId.of("Asia/Shanghai");
		ZonedDateTime zdt = ins.atZone(zid);

		LocalTime ldt1 = zdt.toLocalTime();

		return ldt1;
	}

	// calendar计算两个date天数
	public long calenderDayOf(Date d1, Date d2) {
		Calendar c1 = typeToCalendar(d1);
		Calendar c2 = typeToCalendar(d2);

		return Math.abs(c1.getTimeInMillis() - c2.getTimeInMillis()) / 1000 / 60 / 60 / 24;
	}

	// localdatetime计算两个date天数
	public long datetimeDayOf(Date d1, Date d2) {
		LocalDate ldt1 = typeToDate(d1);
		LocalDate ldt2 = typeToDate(d2);

		return Math.abs(ldt1.toEpochDay() - ldt2.toEpochDay());
	}

	// calendar计算两个date秒数
	public long calenderSecondOf(Date d1, Date d2) {
		Calendar c1 = typeToCalendar(d1);
		Calendar c2 = typeToCalendar(d2);

		return Math.abs(c1.getTimeInMillis() - c2.getTimeInMillis()) / 1000;
	}

	// localdatetime计算两个date秒数
	public long datetimeSecondOf(Date d1, Date d2) {
		LocalTime ldt1 = typeToTime(d1);
		LocalTime ldt2 = typeToTime(d2);

		LocalDate ld1 = typeToDate(d1);
		LocalDate ld2 = typeToDate(d2);

		ZoneOffset zo = ZoneOffset.of("+08:00");

		return Math.abs(ldt1.toEpochSecond(ld1, zo) - ldt2.toEpochSecond(ld2, zo));
	}

	// 计算生肖
	public String zodiacSign(Date d) {
		DateFormat df = new SimpleDateFormat("yyyy");

		String year = df.format(d);

		LocalDate date = typeToDate(d);

		char animal = ANIMAL.charAt(date.getYear() % 12 - 4 > 0 ? date.getYear() % 12 - 4 : date.getYear() % 12 + 8);

		String gzjn = ""
				+ TIANGAN.charAt((date.getYear() % 10 - 4) > 0 ? date.getYear() % 10 - 4 : date.getYear() % 10 + 6)
				+ DIZHI.charAt((date.getYear() % 12 - 4) > 0 ? date.getYear() % 12 - 4 : date.getYear() % 12 + 8);

		return year + "年为" + gzjn + "年" + "生肖" + animal;

	}
}
