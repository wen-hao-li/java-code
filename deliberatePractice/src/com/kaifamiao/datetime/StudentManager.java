package com.kaifamiao.datetime;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class StudentManager {
	private List<Student> list=new ArrayList<Student>();
	private static Scanner s = new Scanner(System.in);
	private static DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
	public static void main(String[] args) {
		System.out.println("欢迎使用学生管理系统");
		StudentManager sm=new StudentManager();
		System.out.println("- - -".repeat(5));
		boolean flag = true;
		while (flag) {
			System.out.println("- - -".repeat(5));
			System.out.println("01.查询所有学生信息");
			System.out.println("02.输入ID查询学生信息");
			System.out.println("03.输入姓名查询学生信息");
			System.out.println("04.添加学生信息");
			System.out.println("05.输入ID删除学生信息");
			System.out.println("06.修改学生");
			System.out.println("07.退出系统");
			System.out.println("请输入操作数：");
			int key = s.nextInt();
			switch (key) {
			case 1:
				sm.select();
				break;
			case 2:
				System.out.println("输入查询学生ID");
				int id=s.nextInt();
				try {
					sm.select(id);
				} catch (ExistException e) {
					System.err.println(e.getMessage());
				}
				break;
			case 3:
				System.out.println("输入查询学生姓名");
				String name=s.next();
				try {
					sm.select(name);
				} catch (ExistException e) {
					System.err.println(e.getMessage());
				}
				break;
			case 4:
				try {
					Student stu=sm.buildStu();
					sm.add(stu);
				} catch (ParseException | ExistException e) {
					System.err.println(e.getMessage());
				}
				break;
			case 5:
				System.out.println("输入删除学生ID");
				int delID=s.nextInt();
				try {
					sm.delete(delID);
				} catch (ExistException e) {
					System.err.println(e.getMessage());
				}
				break;
			case 6:
				System.out.println("输入修改学生ID");
				int updateID=s.nextInt();
				try {
					sm.update(updateID);
				} catch (ParseException | ExistException e) {
					System.err.println(e.getMessage());
				}
				break;
			case 7:
				s.close();
				flag = false;
				break;
			default:
				break;
			}
		}
		System.out.println("谢谢使用，再见");
		System.exit(0);
	}
	//创建一个学生
	public Student buildStu() throws ParseException {
		
		System.out.println("输入学生ID");
		int id=s.nextInt();
		System.out.println("输入学生姓名");
		String name=s.next();
		System.out.println("输入学生性别");
		String gender=s.next();
		System.out.println("输入学生生日(格式yyyy-MM-dd)");
		String birth=s.next();
		Date birthDate=df.parse(birth);
		System.out.println("输入学生电话");
		String tel=s.next();
		return new Student(id,name,gender,birthDate,tel);
	}
	//输出学生格式化
	public void stuFormat(Student stu) {
		String bith=df.format(stu.getBirthdate());
		System.out.println(
				stu.getId()+"\t"
				+stu.getName()+"\t"
				+stu.getGender()+"\t"
				+bith+"\t"
				+stu.getTel()+"\t"
				);
	}
	//查询所有学生
	public void select() {
		if(list.isEmpty()) {
			System.out.println("现在还没有学生加入哦~");
		}
		System.out.println("ID\t姓名\t性别\t生日\t\t电话");
		Iterator<Student> iter=list.iterator();
		while(iter.hasNext()) {
			stuFormat(iter.next());
		}
	}
	//按ID查询学生
	public void select(int id) throws ExistException {
		if(exist(id)!=null) {
			System.out.println("ID\t姓名\t性别\t生日\t\t电话");
			stuFormat(exist(id));
		}
		else {
			throw new ExistException("没有对应ID的学生");
		}
	}
	//按照姓名查询学生
	public void select(String name) throws ExistException {
		if(!exist(name).isEmpty()) {
			System.out.println("ID\t姓名\t性别\t生日\t\t电话");
			Iterator<Student> iter=exist(name).iterator();
			while(iter.hasNext()) {
				stuFormat(iter.next());
			}
		}else {
			throw new ExistException("没有对应名字的学生");
		}
	}
	//是否存在对应ID学生，存在返回学生，不存在返回null
	private Student exist(int id) {
		Iterator<Student> iter=list.iterator();
		while(iter.hasNext()) {
			Student stu=iter.next();
			if(stu.getId()==id) {
				return stu;
			}
		}
		return null;
	}
	//是否存在对应name学生，返回一个学生集合
	private List<Student>  exist(String name) {
		List<Student> selectList=new ArrayList<Student>();
		Iterator<Student> iter=list.iterator();
		while(iter.hasNext()) {
			Student stu=iter.next();
			if(stu.getName().equals(name)) {
				selectList.add(stu);
			}
		}
		return selectList;
	}
	//添加学生
	public void add(Student s) throws ExistException {
		if(exist(s.getId())!=null) {
			throw new ExistException("输入重复");
		}
		list.add(s);
	}
	//按id删除学生
	public void delete(int id) throws ExistException {
		if(exist(id)!=null) {
			if(list.remove(exist(id))) {
				System.out.println("删除成功！");
			}
		}
		else{
			throw new ExistException("没有找到对应ID");
		}
	}
	//按照id修改学生信息
	public void update(int id) throws ParseException, ExistException {
		if(exist(id)!=null) {
			boolean flag=true;
			while(flag) {
				System.out.println("- - -".repeat(5));
				System.out.println("输入操作数来修改学生的对应信息");
				System.out.println("1.修改姓名");
				System.out.println("2.修改性别");
				System.out.println("3.修改生日");
				System.out.println("4.修改电话");
				System.out.println("5.取消修改");
				int key=s.nextInt();
				switch(key) {
				case 1:
					System.out.println("输入姓名");
					String name=s.next();
					exist(id).setName(name);
					break;
				case 2:
					System.out.println("输入性别");
					String gender=s.next();
					exist(id).setGender(gender);
					break;
				case 3:
					System.out.println("输入生日");
					String birth=s.next();
					Date birthDate=df.parse(birth);
					exist(id).setBirthdate(birthDate);
					break;
				case 4:
					System.out.println("输入电话");
					String tel=s.next();
					exist(id).setTel(tel);
					break;
				case 5:
					flag=false;
					break;
				default:
					System.err.println("请输入真确操作数");	
				}
				System.out.println("是否继续？y/n");
				String str=s.next();
				if(str.equals("n")) {
					flag=false;
				}
			}
			
		}
		else {
			throw new ExistException("没有找到对应ID");
		}
	}
}
