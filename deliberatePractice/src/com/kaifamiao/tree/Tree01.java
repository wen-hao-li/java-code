package com.kaifamiao.tree;

public class Tree01 {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		Node node1=new Node('A');
		Node node2=new Node('B');
		Node node3=new Node('C');
		Node node4=new Node('D');
		Node node5=new Node('E');
		Node node6=new Node('F');
		Node node7=new Node('G');
		node1.setLeft(node2);
		node1.setRight(node3);
		node2.setLeft(node4);
		node2.setRight(node5);
		node3.setLeft(node6);
		node6.setRight(node7);
		System.out.println("先序遍历");
		proShow(node1);
		System.out.println("中序遍历");
		midShow(node1);
		System.out.println("后序遍历");
		lastShow(node1);
	}
	//先序
	public static void proShow(Node node) {
		System.out.println(node.getItem());
		if(node.getLeft()!=null) {
			proShow(node.getLeft());
		}
		if(node.getRight()!=null) {
			proShow(node.getRight());
		}
	}
	//中序
	public static void midShow(Node node) {
		if(node.getLeft()!=null) {
			midShow(node.getLeft());
		}
		System.out.println(node.getItem());
		if(node.getRight()!=null) {
			midShow(node.getRight());
		}
	}
	//后序
	public static void lastShow(Node node) {
		if(node.getLeft()!=null) {
			lastShow(node.getLeft());
		}
		if(node.getRight()!=null) {
			lastShow(node.getRight());
		}
		System.out.println(node.getItem());
	}
}
