package com.kaifamiao.tree;

public class Node {
	private Node left;
	private char item;
	private Node right;
	public Node(char item) {
		super();
		this.item = item;
	}
	public Node getLeft() {
		return left;
	}
	public void setLeft(Node left) {
		this.left = left;
	}
	public char getItem() {
		return item;
	}
	public void setItem(char item) {
		this.item = item;
	}
	public Node getRight() {
		return right;
	}
	public void setRight(Node right) {
		this.right = right;
	}
	
}
