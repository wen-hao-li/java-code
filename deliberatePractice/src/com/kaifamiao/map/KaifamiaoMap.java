package com.kaifamiao.map;


import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class KaifamiaoMap<K,V> implements Map<K, V> {
	
	private static class SimpleEntry<K,V> implements Map.Entry<K, V> {
		@SuppressWarnings("unused")
		final int hash ;
		final K key ;
		V value ;
		@SuppressWarnings("unused")
		SimpleEntry<K,V> next ;
		
		public SimpleEntry( int hash , K key, V value , SimpleEntry<K,V> next) {
			super();
			this.hash = hash ;
			this.key = key;
			this.value = value;
			this.next = next ;
		}

		@Override
		public K getKey() {
			return this.key ;
		}

		@Override
		public V getValue() {
			return this.value ;
		}

		@Override
		public V setValue(V value) {
			V oldValue = this.value ;
			this.value = value ;
			return oldValue;
		}
		
	}
	
	private static final int DEFAULT_CAPACITY = 16 ;
	private static final float DEFAULT_FACTOR = 0.75F ;
	private int counter ; // 统计 映射项(键值对) 个数
	private SimpleEntry<K,V>[] table ; // 用来存放 映射项 的数组
	private float factor ; // 加载因子
	
	@SuppressWarnings("unchecked")
	public KaifamiaoMap() {
		super();
		this.table = new SimpleEntry[ DEFAULT_CAPACITY ];
		this.factor = DEFAULT_FACTOR ;
	}
	
	// 从 HashMap 抄过来的方法
	static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

	@Override
	public V put(K key, V value) {
		// 首先确定 key 对应的 Entry 是否存在于 table 中
		
		// 如果 key 对应的 Entry 在 table 不存在，则创建新的 Entry 实例
		int h = hash( key );
		SimpleEntry<K,V> entry = new SimpleEntry<>( h , key , value , null ); 
		int i = ( table.length - 1 ) & h ;
		System.out.println( i );
		SimpleEntry<K,V> p = table[ i ] ;
		if( p == null ) {
			counter++;
			if(counter>=table.length*factor) {
				System.arraycopy(table, 0, table, 0, table.length*2);
			}
			table[ i ] = entry ;
			return null ;
		} 
		// 如果索引 i 处已经存在元素
		
		entry.next = p ; // 则让新的Entry指向数组中原来存在的Entry
		table[ i ] = entry ; // 将新的Entry添加到数组中
		return null;
	}

	@Override
	public V get(Object key) {
		return null;
		
	}

	@Override
	public V remove(Object key) {
		return null;
	}
	
	@Override
	public void clear() {
	}
	
	@Override
	public int size() {
		return counter ;
	}

	@Override
	public boolean isEmpty() {
		return counter == 0 ;
	}
	
	@Override
	public boolean containsKey(Object key) {
		return false;
	}

	@Override
	public boolean containsValue(Object value) {
		return false;
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
	}
	
	@Override
	public Set<K> keySet() {
		return null;
	}

	@Override
	public Collection<V> values() {
		return null;
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		return null;
	}

	@Override
	public String toString() {
		// 先遍历数组，如果数组某个元素存在，则判断是否存在下一个元素
		return "KaiFaMiaoMap [toString()=" + super.toString() + "]";
	}

}