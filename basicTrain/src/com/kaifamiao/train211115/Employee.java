package com.kaifamiao.train211115;

import java.util.Scanner;

public class Employee {
	private double sal;
	public static void main(String[] args) {
		Employee e=new Employee();
		System.out.println(e.payTaxes());
	}
	public Employee() {
		super();
		getSal();
	}
	public void getSal() {
		Scanner s=new Scanner(System.in);
		System.out.println("输入金额");
		sal=s.nextDouble();
		s.close();
	}
	public double payTaxes() {
		if(sal<=5000) {
			return 0;
		}
		else if(sal>5000&& sal<=8000){
			return (sal-5000)*0.03;
		}
		else if(sal>8000&& sal<=17000){
			return (sal-5000)*0.1-(2520/12);
		}
		else if(sal>17000&& sal<=25000){
			return (sal-5000)*0.2-(16920/12);
		}
		else if(sal>25000&& sal<=40000){
			return (sal-5000)*0.25-(31920/12);
		}
		else if(sal>40000&& sal<=60000){
			return (sal-5000)*0.3-(52920/12);
		}
		else if(sal>60000&& sal<=85000){
			return (sal-5000)*0.35-(85920/12);
		}
		else{
			return (sal-5000)*0.45-(181920/12);
		}
		
	}
}
