package com.kaifamiao.train211111;

import java.util.Scanner;

public class Alphabet {
	public void upAlphabet(char ch) {
		if(ch>='a'&&ch<='z') {
			char chUp=(char)(ch-32);
			System.out.println("你输入的是"+ch+",对应的大写英文是"+chUp);
			return;
		}
		if(ch>='A'&&ch<='Z') {
			System.out.println("你输入的大写英文字母"+ch);
			return;
		}
		System.out.println("你输入的不是英文字母");
	}
	public static void main(String args[]) {
		Alphabet a=new Alphabet();
		Scanner s=new Scanner(System.in);
		String line=s.nextLine();
		for(int i=0;i<line.length();i++) {
			char c=line.charAt(i);
			a.upAlphabet(c);
		}
		s.close();
	}
}
