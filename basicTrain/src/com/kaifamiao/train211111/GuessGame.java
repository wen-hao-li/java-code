package com.kaifamiao.train211111;

import java.util.Random;
import java.util.Scanner;

public class GuessGame {
	public void numberGuess() {
		Random r=new Random();
		Scanner s=new Scanner(System.in);
		int key=r.nextInt(1000);
		while(true) {
			System.out.print("输入猜的数：[0-1000)");
			int guess=s.nextInt();
			if(guess>key) {
				System.out.println("太大了，再小点");
				continue;
			}
			if(guess<key) {
				System.out.println("太小了，再大点");
				continue;
			}
			s.close();
			break;
		}
		System.out.println("恭喜猜对是"+key);
	}
	public static void main(String[] args) {
		GuessGame game=new GuessGame();
		game.numberGuess();
	}
}
