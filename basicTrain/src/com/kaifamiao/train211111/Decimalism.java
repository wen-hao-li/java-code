package com.kaifamiao.train211111;

import java.util.Scanner;

public class Decimalism {
	public double converter(String str) {
		char[] ch=str.toCharArray();
		for(char c:ch) {
			if(c!='1'&&c!='0') {
				System.out.println("输入不是一个二进制");
				return -1;
			}
		}
		double result=0;
		for(int i=0;i<ch.length;i++) {
			if(ch[i]=='1') {
				result+=Math.pow(2, ch.length-1-i);
			}
		}
		return result;
	}
	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		Decimalism d=new Decimalism();
		Scanner s=new Scanner(System.in);
		String str = s.nextLine();
		s.close();
		System.out.println("输入的二进制是"+str);
		double n=d.converter(str);
		if(n>=0) {
			System.out.println("十进制是"+n);
		}	
	}
}
