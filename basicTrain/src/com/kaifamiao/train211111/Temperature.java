package com.kaifamiao.train211111;

import java.util.Scanner;

public class Temperature {
	public double converter(double f) {
		return 5*(f-32)/9;
	}
	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("请输入华氏温度");
		double f=s.nextDouble();
		Temperature t=new Temperature();
		double c=t.converter(f);
		System.out.println("输入的华氏温度是"+f+"，摄氏温度是"+c);
		s.close();
	}
}
