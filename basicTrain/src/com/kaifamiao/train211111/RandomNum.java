package com.kaifamiao.train211111;


import java.util.Random;

public class RandomNum {
	public int[] randomArr() {
		int[] arr=new int[7];
		Random r=new Random();
		int i=0;
		while(true) {
			if(i>6) {
				break;
			}
			boolean flag=true;
			int key=r.nextInt(33)+1;
			for(int num:arr) {
				if(num==key) {
					flag=false;
				}
			}
			if(flag) {
				arr[i++]=key;
				continue;
			}
		}
		return arr;
	}
	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		RandomNum rNum=new RandomNum();
		int[] arr=rNum.randomArr();
		for(int num:arr) {
			System.out.print(num+"\t");
		}
	}
}
