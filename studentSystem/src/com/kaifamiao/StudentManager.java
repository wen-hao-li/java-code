package com.kaifamiao;

import java.util.Arrays;
import java.util.Scanner;

public class StudentManager {
	Student[] students = new Student[5];
	static int pos = 0;
	//创建学生
	public Student buildStu() {
		@SuppressWarnings("resource")
		Scanner s=new Scanner(System.in);
		System.out.println("请输入学生学号");
		int id=s.nextInt();
		System.out.println("请输入学生姓名");
		String name=s.next();
		System.out.println("请输入学生年龄");
		int age=s.nextInt();
		
		Student stu=new Student(id,name,age);
		return stu;
	}
	// 添加学生
	public void add(Student stu) {
		if (pos >= students.length) {
			Student[] stu1 = Arrays.copyOf(students, students.length * 2);
			students = stu1;
		}
		for(Student stu1:students) {
			if(stu1==null) {
				continue;
			}
			if(stu.getId()==stu1.getId()) {
				System.out.println("学号已存在学生，请重新添加");
				return;
			}
		}
		students[pos++] = stu;
		System.out.println("添加成功");
	}

	// 根据ID删除数组中学生
	public void remove(int id) {
		for (int i = 0; i < pos; i++) {
			if (id == students[i].getId()) {
				pos--;
				students[i] = null;
				System.arraycopy(students, i + 1, students, i, students.length - i - 1);
				System.out.println("删除成功");
				return;
			}
		}
		System.out.println("删除失败，没有对应ID的学生");
	}

	// 根据name删除数组中学生
	public void remove(String name) {
		for (int i = 0; i < pos; i++) {
			if (name.equals(students[i].getName())) {
				pos--;
				students[i] = null;
				System.arraycopy(students, i + 1, students, i, students.length - i - 1);
				System.out.println("删除成功");
				return;
			}
		}
		System.out.println("删除失败，没有对应姓名的学生");
	}
	//判断ID是否存在
	public boolean idExist(int id) {
		for(Student stu:students) {
			if(stu==null) {
				continue;
			}
			if(id==stu.getId()) {
				return true;
			}
		}
		return false;
	}
	// 根据ID修改指定学生
	public void update4Id(int id) {
		if(!idExist(id)) {
			System.out.println("没有找到对应ID学生");
			return;
		}
		for(int i=0;i<pos;i++) {
			if(students[i].getId()==id) {
				Student stu=students[i];
				students[i]=null;
				Student stu1=buildStu();
				if(idExist(stu1.getId())) {
					students[i]=stu;
					System.out.println("修改失败");
					return;
				}
				students[i]=stu1;
				System.out.println("修改成功");
				return;
			}
		}
	}

	// 显示数组中学生
	public void show() {
		for (Student stu : students) {
			if (stu != null)
				System.out.println(stu);
		}
	}

	// 显示指定id的学生
	public void show(int id) {
		if(!idExist(id)) {
			System.out.println("没有找到对应id学生");
			return ;
		}
		for (int i = 0; i < pos; i++) {
			if (students[i].getId()==id) {
				System.out.println(students[i]);
			}
		}
	}
}
