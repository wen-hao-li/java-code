package com.kaifamiao;

import java.util.Scanner;

public class StudentManagerTest {

	public static void main(String[] args) {

//		StudentManager s=new StudentManager();
//		Student stu1=new Student(1,"宋江",45);
//		Student stu2=new Student(2,"林冲",27);
//		Student stu3=new Student(3,"李逵",33);
//		Student stu4=new Student(4,"鲁智深",30);
//		Student stu5=new Student(5,"吴用",40);
//		Student stu6=new Student(6,"孙二娘",28);
//		Student stu7=new Student(7,"燕青",25);
//		Student stu8=new Student(8,"扈三娘",30);
//		Student stu9=new Student(9,"秦明",40);
//		Student stu10=new Student(10,"武松",26);
//		Student stu11=new Student(11,"武大郎",30);
//		Student stu12=new Student(12,"镇关西",32);
//		Student stu13=new Student(13,"九纹龙",41);
//		Student stu14=new Student(14,"急先锋",42);
//		Student stu15=new Student(15,"青面兽",35);

		Scanner scanner=new Scanner(System.in);
		boolean flag=true;
		StudentManager s=new StudentManager();
		System.out.println("欢迎使用学生管理系统");
		while(flag) {
			System.out.println("请输入操作数");
			System.out.println("1.添加学生");
			System.out.println("2.查看学生列表");
			System.out.println("3.删除对应ID学生");
			System.out.println("4.删除对应姓名学生");
			System.out.println("5.修改对应ID学生信息");
			System.out.println("6.查询对应ID学生");
			System.out.println("7.退出");
			int key=scanner.nextInt();
			switch(key) {
			case 1:
				Student stu=s.buildStu();
				s.add(stu);
				break;
			case 2:
				s.show();
				break;
			case 3:
				System.out.println("输入ID");
				int id=scanner.nextInt();
				s.remove(id);
				break;
			case 4:
				System.out.println("输入姓名");
				String name=scanner.next();
				s.remove(name);
				break;
			case 5:
				System.out.println("输入学号");
				int upId=scanner.nextInt();
				s.update4Id(upId);
				break;
			case 6:
				System.out.println("输入ID");
				int checkId=scanner.nextInt();
				s.show(checkId);
				break;
			case 7:
				flag=false;
				scanner.close();
				break;
			default:
				System.out.println("请输入正确操作数");
				break;
			}
			
		}
		System.out.println("谢谢使用再见");
	}

}
