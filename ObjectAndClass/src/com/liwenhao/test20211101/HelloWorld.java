package com.liwenhao.test20211101;

public class HelloWorld {
	
	public static void main(String[] args) {
		// TODO 自动生成的方法存根
	
		Triangle t = new Triangle();
		t.firstEdge = 30 ;
		t.secondEdge = 40 ;
		t.thirdEdge = 50 ; 
		t.squaring() ; // 计算面积并在控制台输出面积
		
		Cylinder  c = new Cylinder();
		c.radius = 20 ;
		c.height = 20 ;
		c.squaring() ; // 计算圆柱体表面积并在控制台输出面积
		c.cube() ; // 计算圆柱体体积并在控制台输出体积
		
	}

}
