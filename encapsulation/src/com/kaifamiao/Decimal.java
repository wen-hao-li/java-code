package com.kaifamiao;

public class Decimal {
	private final static String number="0123456789ABCDEFGHIJKLMN";
	private final long value;
	
	public Decimal(long value) {
		this.value = value;
		
	}
	public String toString(int num) {
		long val=value;
		String str="";
		while(val!=0) {
			str=number.charAt((int) (val%num))+str;
			val=val/num;
		}
		
		return str;
	}
	public String toBinaryString() {
		
		return toString(2);
	}
	public String toOctString() {
		return toString(8);
	}

	public String toHexString() {
		return toString(16);
	}
}
