package com.kaifamiao;

import java.util.ArrayList;
import java.util.List;

public class BinaryFindTest {
	public static void main(String[] args) {
		int[] arr= {-1,1,5,14,25,25,50,77,95,100,100};
		BinaryFind f=new BinaryFind();
		List<Integer> list=f.find(arr, 108, 0, arr.length-1);
		System.out.println(list);
	}
}
class BinaryFind{
	public List<Integer> find(int[] arr,int x,int left,int right) {
		if(left>right) {
			return new ArrayList<Integer>();
		}
		int mid=(left+right)/2;
		int midV=arr[mid];
		if(x>midV){
			return find(arr,x,mid+1,right);
		}else if(x<midV) {
			return find(arr,x,left,mid-1);
		}else {
			List<Integer> list=new ArrayList<Integer>();
			int temp=mid-1;
			while(true) {
				if(temp<0||arr[temp]!=midV) {
					break;
				}
				list.add(temp);
				temp--;
			}
			list.add(mid);
			temp=mid+1;
			while(true) {
				if(temp>arr.length-1||arr[temp]!=midV) {
					break;
				}
				list.add(temp);
				temp++;
			}
			return list;
		}	
		
	}
}