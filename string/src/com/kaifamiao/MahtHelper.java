package com.kaifamiao;

public class MahtHelper {
	private final static String key="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		MahtHelper m=new MahtHelper();
		System.out.println(m.toString(42,2));
		System.out.println(m.toInteger("101010",2));
		System.out.println(m.toString(128,8));
		System.out.println(m.toInteger("200",8));
		System.out.println(m.toString(186,16));
		System.out.println(m.toInteger("BA",16));
		System.out.println(m.toString(548,32));
		System.out.println(m.toInteger("H4",32));
	}
	public String toString(long integer ,int radix) {
		if(integer<0 || (radix<2||radix>36)) {
			throw new RuntimeException();
		}
		String res="";
		long num=integer;
		while(num!=0) {
			res=key.charAt((int) (num%radix))+res;
			num=num/radix;
		}
		return res;
	}
	public  long toInteger(String origin,int radix) {
		if(origin==null) {
			throw new NullPointerException();
		}
		if(origin=="" || (radix<2||radix>36) ) {
			throw new RuntimeException();
		}
		long num=0;
		for(int i=0;i<origin.length();i++) {
			long t=key.indexOf(origin.charAt(origin.length()-1-i));
			num+=Math.pow(radix, i)*(t);
		}
		return num;
	}
}
