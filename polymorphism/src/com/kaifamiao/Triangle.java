package com.kaifamiao;

public class Triangle extends Shape {
	public double firstSide;
	public double secondSide;
	public double thirdSide;

	public Triangle(double firstSide, double secondSide, double thirdSide) {
		this.type = "三角形";
		this.firstSide = firstSide;
		this.secondSide = secondSide;
		this.thirdSide = thirdSide;
	}

	
	public double calculate() {
		double p=(firstSide+secondSide+thirdSide)/2;
		area=Math.sqrt(p*(p-firstSide)*(p-secondSide)*(p-thirdSide));
		return area;
	}

	public void description() {
		System.out.println(this.type+"的三边长分别是"+this.firstSide+","+this.secondSide+","+this.thirdSide);
	}
}
