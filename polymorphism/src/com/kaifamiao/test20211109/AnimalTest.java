package com.kaifamiao.test20211109;

public class AnimalTest {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		Animal a = null ;
		a = new Phoenix("凤凰传奇",12);
		System.out.println( a.toString() );
		a.eat( "猪肉" );

		a = new Eagle("鸽子",21);
		System.out.println( a.toString() );
		a.eat(  "牛肉" );

		a = new Bear("倒霉熊",18);
		System.out.println( a.toString() );
		a.eat(  "菠菜"  );

		a = new Lion("辛巴",24);
		System.out.println( a.toString() );
		a.eat( "白菜" );
	}

}
