package com.kaifamiao.test20211109;

public class Animal {
	private final String type;
	protected String name;
	protected int age;
	public Animal(String type) {
		this.type=type;
	}
	public Animal(String type ,int age) {
		this.age=age;
		this.type=type;
	}
	public Animal( String type ,String name, int age ) {
        this.type = type ;
        this.age = age ;
        this.name=name;
    }
	public void eat(String food) {
		System.out.println("动物可以吃"+food);
	}
	@Override
	public String toString() {
		return "Animal [type=" + type + ", name=" + name + ", age=" + age + "]";
	}
}
