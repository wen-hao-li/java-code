package com.kaifamiao.test20211109;

public class SortAnimalTest {
	public static void main(String[] args) {
		Animal[] animals = new Animal[ 10 ] ; 

		animals[ 0 ] = new Eagle("飞鸽",12 );
		animals[ 1 ] = new Eagle("信鸽",14 );
		animals[ 2 ] = new Eagle("鸽子蛋",19 );
		animals[ 3 ] = new Lion("辛巴",24 );
		animals[ 4 ] = new Phoenix("凤凰传奇",34 );
		animals[ 5 ] = new Phoenix("凤凰奇传",43 );
		animals[ 6 ] = new Bear("熊大",24 );
		animals[ 7 ] = new Bear("熊二",18 );
		animals[ 8 ] = new Bear("倒霉熊",18 );
		animals[ 9 ] = new Lion("狮子喵",30 );
		System.out.println("排序前");
		for(Animal am:animals) {
			System.out.println(am);
		}
		
		int len =animals.length;
		Animal temp=null;
		for(int i=0;i<len-1;i++) {
			for(int j=0;j<len-1-i;j++) {
				if(animals[j].age> animals[j+1].age) {
					temp=animals[j+1];
					animals[j+1]=animals[j];
					animals[j]=temp;
					
				}
			}
		}
		System.out.println("排序后");
		for(Animal am:animals) {
			System.out.println(am);
		}
	}
}
