package com.kaifamiao;

public class Circle extends Shape{
	public double r;
	public Circle(double r){
		this.type="圆";
		this.r=r;
	}
	public double calculate() {
		
		this.area= Math.PI*Math.pow(r, 2);
		return area;
	}

	public void description() {
		System.out.println(this.type+"的半径是"+this.r);
	}
}
