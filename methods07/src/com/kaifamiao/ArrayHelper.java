package com.kaifamiao;

public class ArrayHelper {

	public static boolean equal( int[] first , int[] second ) {
		if(first.length!=second.length) {
			return false;
		}
		for(int i=0;i<first.length;i++) {
			if(first[i]!=second[i]) {
				return false;
			}
		}
		return true;
	    
	}
	public static boolean equal( char[] first , char[] second ) {
		if(first.length!=second.length) {
			return false;
		}
		for(int i=0;i<first.length;i++) {
			if(first[i]!=second[i]) {
				return false;
			}
		}
		return true;
	    
	}
}
