package com.kaifamiao;

public class MathHelper {
	public static MathHelper getInstance(){
		MathHelper h=new MathHelper();
		return h;
	}
	public long max( long first , long... values ) {
		long max=first;
		for(int i=0;i<values.length;i++) {
			if(max<values[i]) {
				max=values[i];
			}
		}
		return max;
	    // 找出参数中的最大值并返回
	}
	public double max( double first , double... values ) {
		
		double max=first;
		for(int i=0;i<values.length;i++) {
			if(max<values[i]) {
				max=values[i];
			}
		}
		return max;
	    // 找出参数中的最大值并返回
	}
	public long min( long first , long... values ) {
		long min=first;
		for(int i=0;i<values.length;i++) {
			if(min>values[i]) {
				min=values[i];
			}
		}
		return min;
	    // 找出参数中的最小值并返回
	}
//	public int max(Object object1,Object...objects) {
//		int max=(int)object1;
//		for(int i=0;i<objects.length;i++) {
//			int temp=(int)objects[i];
//			if(max<temp) {
//				max=temp;
//			}
//		}
//		return max;
//	}
	public double min( double first , double... values ) {
		double min=first;
		for(int i=0;i<values.length;i++) {
			if(min>values[i]) {
				min=values[i];
			}
		}
		return min;
	    // 找出参数中的最小值并返回
	}
}
