package com.kaifamiao;

public class Sinaean {
	public Sinaean(String name, char gender, int age, boolean married) {
		super();
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.married = married;
	}
	String name;
	char gender;
	int age;
	boolean married;
	public void marry( Sinaean another ) {
	    // 在这里填空，判断自己 ( this ) 跟 另外一个人( another )结婚
		//判断双方是否已婚
		if(this.married||another.married) {
			System.out.println("有一方已婚，不能结婚");
			return;
		}
		//判断双方是否同性
		if(this.gender==another.gender) {
			System.out.println("同性不允许结婚");
			return;
		}
		//判断双方年龄是否满足
		if(this.gender=='男') {
			if(this.age<22 || another.age<20) {
				System.out.println("年龄不够，不允许结婚");
				return;
			}
		}else {
			if(this.age<20 || another.age<22) {
				System.out.println("年龄不够，不允许结婚");
				return;
			}
		}
		System.out.println("恭喜"+this.name+"可以和"+another.name+"结婚");
		
		
	}
	
}
