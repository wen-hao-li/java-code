package com.kaifamiao;

public class Sorter {
	
	public void traversal( int[] array ){
		//判断数组是否是空数组
		if(array.length==0) {
	    	System.out.println("空数组");
	    	return;
	    }
		//判断数组中是否有值
		boolean flag=true;
	    for(int i=0;i<array.length;i++) {
	    	if(array[i]!=0) {
	    		flag=false;
	    		break;
	    	}	
	    }
	    if(flag) {
	    	System.out.println("数组为null");
	    	return;
	    }
	    //打印数组中的值
	    for(int i=0;i<array.length-1;i++) {
	    	System.out.print(array[i]+",");
	    	if(i==array.length-2) {
	    		System.out.print(array[i+1]);
	    	}	
	    }
	}
	//排序，顺序排序
	public void sort( int[] array ) {
	    int temp;
	    for(int i=0;i<array.length;i++) {
	    	int maxPos=i;
	    	for(int j=i;j<array.length;j++) {
	    		if(array[j]>array[maxPos]) {
	    			maxPos=j;
	    		}
	    	}
	    	temp=array[i];
	    	array[i]=array[maxPos];
	    	array[maxPos]=temp;
	    }
	}
}
