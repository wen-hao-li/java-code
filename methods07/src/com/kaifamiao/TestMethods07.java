package com.kaifamiao;

public class TestMethods07 {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根

		Sorter sorter = new Sorter();

		int[] a = { 1, 100, -20, 99, 1000, 0, 30 };
		System.out.print("排序前 : ");
		sorter.traversal(a); // 排序前 : 1 , 100 , -20 , 99 , 1000 , 0 , 30

		sorter.sort(a);
		System.out.print("排序后 : ");
		sorter.traversal(a); // 排序后 : -20 , 0 , 1 , 30 , 99 , 100 , 1000

		System.out.println();
		System.out.println("*************");
		Sinaean sinaean1 = new Sinaean("李逵", '男', 25, false);
		Sinaean sinaean2 = new Sinaean("孙二娘", '女', 25, false);
		Sinaean sinaean3 = new Sinaean("武大郎", '男', 25, true);
		Sinaean sinaean4 = new Sinaean("红孩儿", '女', 11, false);
		sinaean1.marry(sinaean2);
		sinaean1.marry(sinaean3);
		sinaean1.marry(sinaean4);

		System.out.println("*************");
		int[] aa = { 1, 2 };
		int[] bb = { 1, 2 };
		char[] cc = { 'a', 'b' };
		char[] dd = { 'a', 'b' };

		System.out.println(ArrayHelper.equal(aa, bb));
		System.out.println(ArrayHelper.equal(cc, dd));

		System.out.println("**************");
		
		MathHelper mh = MathHelper.getInstance();
		
		long n = 99L;
		short s = 77;
		byte b = 66;
		char c = 'x';
		long x = mh.max(n, s, b, c);
		System.out.println(x);

		System.out.println("**************");

		System.out.println();
		Chronology.show();
		String name = Chronology.seek(1985);
		System.out.println(name); // 输出 乙丑
		char ss = Chronology.sign(1996);
		System.out.println(ss); // 输出 鼠
	}

}
