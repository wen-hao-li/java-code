package com.kaifamiao;

public class Chronology {
	// private 表示私有的
	private static final char[]HEAVENLY_STEMS={'甲','乙' , '丙' , '丁' , '戊' , '己' , '庚' , '辛' , '壬' , '癸' }; // 天干
	// static 表示与类相关的、属于类的
	private static final char[]EARTHLY_BRANCHES={ '子' , '丑' , '寅' , '卯' , '辰' , '巳' ,  '午' , '未' , '申' , '酉' , '戌' , '亥' }; // 地支
	// final 表示最终的、不可更改的
	private static final char[]CHINESE_ZODIAC={ '鼠','牛','虎','兔','龙','蛇','马','羊','猴','鸡','狗','猪' };  // 生肖
	private static final int CONTRAST = 1984 ; 
	public static void show(){
		for(int i=0;i<60;i++) {
			System.out.print(""+HEAVENLY_STEMS[i%10]+EARTHLY_BRANCHES[i%12]+"\t");
			if(i%10==9) {
				System.out.println();
			}
		}
	}
	public static String seek( int year ) {
	    int y=year-CONTRAST;
	    String str=""+HEAVENLY_STEMS[y%10]+EARTHLY_BRANCHES[y%12];
	    return str;
	}
	public static char sign( int year ) {
		int y=year-CONTRAST;
		return CHINESE_ZODIAC[y%12];
	}
}
