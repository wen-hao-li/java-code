package com.liwenhao.test1101;

import java.util.Arrays;

public class QuickSortArray {
	public static void main(String[] args) {
		int [] arr= {100,125,800,45,-2,1,800,18,37,54};
		System.out.println(Arrays.toString(arr));
		QuickSort qs=new QuickSort();
		qs.sortArr(arr,0,arr.length-1);
		System.out.println(Arrays.toString(arr));
	}
}
class QuickSort{

	public void sortArr(int[] arr, int i, int length) {
		int left=i;
		int right=length;
		int mid =arr[(left+right)/2];
		while(left<right) {
			while(arr[left]<mid) {
				left++;
			}
			while(arr[right]>mid) {
				right--;
			}
			if(left>=right) {
				break;
			}
			int temp;
			temp=arr[right];
			arr[right]=arr[left];
			arr[left]=temp;
			if(arr[left]==mid) {
				right--;
			}
			if(arr[right]==mid) {
				left++;
			}
		}
		if(left==right) {
			left++;
			right--;
		}
		if(left<length) {
			sortArr(arr,left,length);
		}
		if(right>i) {
			sortArr(arr,i,right);
		}	
	}	
}