package com.liwenhao.test1101;

import java.util.Arrays;

// 比较数组是否相等
public class EqualArray {
	
	public static void main(String[] args) {
	    int[] first = {1, 3, 5, 7, 9};
	    int[] second = {1, 3, 5, 9, 7};
	    int[] third = {1, 3, 5, 7, 9};
	    int[] fourth = {1, 3, 5, 7};
	    
	    boolean x = equal(first, second); // 调用本类定义的类方法时可以省略类名
	    System.out.println(x); // false
	    System.out.println( Arrays.equals( first ,  second ) ); // false
	    
	    x = equal(first, third);
	    System.out.println(x); // true
	    
	    x = equal(first, fourth);
	    System.out.println(x); // false
	}
	
	public static boolean equal(int[] first, int[] second) {
		int len1=first.length;
		if(len1!=second.length) {
			return false;
		}
		for(int i=0;i<len1;i++) {
			if(first[i]!=second[i]) {
				return false;
			}
		}
		// 仅当两个数组中相应位置的元素全部相等时返回 true
		return true ;

	}

}
