package com.liwenhao.test1101;

public class SortArray {

	public static void main(String[] args) {
		
		// 设 names 数组中存放学生姓名
		final String[] names = {"雷军", "马云", "谭浩强", "张朝阳", "丁磊" };
		
		// 设 courses 数组中依次存放三门课程的名称
		final String[] courses = {"C++", "Java", "English"};
		
		// 设 scores 数组中依次存储的是 names 数组中各个学生的 C++ 、Java 、English 课程的成绩
		final int[][] scores = {
		        {90, 89, 75}, 
		        {59, 40, 100}, 
		        {100, 99, 80}, 
		        {80, 61, 61}, 
		        {60, 100, 99}
		};
		
		// 使用 "类名.方法名(参数)" 调用 【类方法】 ( 由 static 修饰的方法 )
		SortArray.show(names, courses, scores);
		
		SortArray.orderByTotal(names, scores);
		
		System.out.println( "- - - - - - - - - - - - - - - - - - - - - - - -" );
		
		SortArray.show(names, courses, scores);

	}
	
	/**
	 * 对单个人的各门课程成绩求和
	 * @param scores 是个一维数组，表示某位同学的各科成绩
	 * @return 返回某位同学各科成绩之和
	 */
	public static int sum( int[] scores ) {
		int sum=0;
		for(int i=0;i<scores.length;i++) {
			sum+=scores[i];
		}
		return sum;
	}
	
	/**
	 * 根据每位学生的总成绩进行排序
	 * @param names 是个一维数组，表示各位同学的姓名
	 * @param scores 是个二维数组，表示所有同学的各科成绩
	 */
	public static void orderByTotal(final String[] names, final int[][] scores) {
		
		String tempName;
		int[] tempSco;
		for(int i=0;i<names.length;i++) {
			int maxSumPos=i;
			for(int j=i;j<names.length;j++) {
				if(sum(scores[j])>sum(scores[maxSumPos])) {
					maxSumPos=j;
				}
			}
			if(maxSumPos!=i) {
				tempName=names[i];
				names[i]=names[maxSumPos];
				names[maxSumPos]=tempName;
				tempSco=scores[i];
				scores[i]=scores[maxSumPos];
				scores[maxSumPos]=tempSco;
			}	
		}	
	}

	/**
	 * 根据每位学生的英语成绩进行排序
	 * @param names 是个一维数组，表示各位同学的姓名
	 * @param scores 是个二维数组，表示所有同学的各科成绩
	 */
	public static void orderByEnglish(final String[] names, final int[][] scores) {
		String tempName;
		int[] tempSco;
		for(int i=0;i<names.length;i++) {
			int maxSumPos=i;
			for(int j=i;j<names.length;j++) {
				if(scores[j][2]>scores[maxSumPos][2]) {
					maxSumPos=j;
				}
			}
			if(maxSumPos!=i) {
				tempName=names[i];
				names[i]=names[maxSumPos];
				names[maxSumPos]=tempName;
				tempSco=scores[i];
				scores[i]=scores[maxSumPos];
				scores[maxSumPos]=tempSco;
			}	
		}
	}

	/**
	 * 输出每位同学的姓名、各科成绩、总成绩
	 * @param names 是个一维数组，表示各位同学的姓名
	 * @param courses 是个一维数组，表示各科目名称
	 * @param scores 是个二维数组，表示所有同学的各科成绩
	 */
	public static void show(final String[] names, final String[] courses, final int[][] scores) {
		System.out.println();
		for(int i=0;i<names.length;i++) {
			System.out.print(names[i]+"\t");
			for(int j=0;j<courses.length;j++) {
				System.out.print(courses[j]+":");
				System.out.print(scores[i][j]);
				System.out.print("    ");
			}
			System.out.print("sum:"+sum(scores[i]));
			System.out.println();
		}
	}
}
