package com.liwenhao.test1101;

public class SixtyYears {
	
	public static void main(String[] args) {
		
		final char[] heavenlyStems = {'甲', '乙', '丙', '丁', '戊', '己', '庚', '辛', '壬', '癸'};
		
		final char[] earthlyBranches = {'子', '丑', '寅', '卯', '辰', '巳', '午', '未', '申', '酉', '戌', '亥'};
		
		// 输出六十甲子名称(要求每行输出12个，总共输出 5 行)

		int len=earthlyBranches.length;
		for(int i=0;i<5;i++) {
			for(int j=0;j<len;j++) {
				System.out.print(heavenlyStems[(j+2*i)%10]);
				System.out.print(earthlyBranches[j]);
				System.out.print("\t");
			}
			System.out.println();
		}
		
	}

}
