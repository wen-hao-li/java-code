package com.kaifamiao.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JDBCDemo {

	public static void main(String[] args) {
		Connection conn = null;
		PreparedStatement pst = null;
		try {
			Class.forName("oracle.jdbc.OracleDriver");
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:KFM","kaifamiao","kaifamiao");
			String sql = "select * from emp";
			pst = conn.prepareStatement(sql);
			ResultSet result = pst.executeQuery();

			List<Employee> list = new ArrayList<>();
			while (result.next()) {
				Employee e = new Employee();
				e.setEmpNo(result.getInt("EMPNO"));
				e.seteName(result.getString("ENAME"));
				e.setJob(result.getString("JOB"));
				e.setMgr(result.getInt("MGR"));
				e.setHireDate(result.getDate("HIREDATE"));
				e.setSal(result.getDouble("SAL"));
				e.setComm(result.getDouble("COMM"));
				e.setDeptNo(result.getInt("DEPTNO"));
				list.add(e);
			}
			for (Employee emp : list) {
				System.out.println(emp);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			try {
				if (conn != null) {
					conn.close();
				}
				if (pst != null) {
					pst.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

}
