package com.kaifamiao.game01;

import java.util.Arrays;
public class Player {
	private double lifeValue;
	private Weapon[] wps=new Weapon[2];
	private String name;
	int number=0;
	//构造
	public Player(double lifeValue, String name) {
		super();
		this.lifeValue = lifeValue;
		this.name = name;
	}
	public Player(double lifeValue, Weapon[] wps, String name) {
		super();
		this.lifeValue = lifeValue;
		this.wps = wps;
		this.name = name;
	}
	//生命值
	public double getLifeValue() {
		return lifeValue;
	}
	public void setLifeValue(double lifeValue) {
		this.lifeValue = lifeValue;
	}
	//武器方法
	public Weapon useWps(int n) {
		return this.wps[n];
	}
	public Weapon[] getWps() {
		return wps;
	}
	public void showWps() {
		System.out.println("玩家"+this.name+"拥有武器:");
		
		for(Weapon w:wps) {
			if(w!=null) {
				System.out.println("武器类型:"+w.wName+"\t"+"武器伤害:"+w.arrackValue);
			}
		}
	}
	public void setWps(Weapon wps) {
		if(number>=this.wps.length) {
			Weapon[] w1 = Arrays.copyOf(this.wps, this.wps.length * 2);
			this.wps = w1;
		}
		this.wps[number]=wps;
		number++;
	}
	//玩家名称
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	//显示信息
	public void display() {
		System.out.println("当前玩家"+this.name+"血量还有"+this.lifeValue);
	}
}
