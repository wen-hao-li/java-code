package com.kaifamiao.game01;

public class Weapon {
	protected Player player;
	protected double arrackValue;
	protected String wName;
	//构造
	public Weapon( String wName,double arrackValue) {
		super();
		this.arrackValue = arrackValue;
		this.wName=wName;
	}
	public Weapon( String wName) {
		super();
		this.wName=wName;
	}
	public Weapon(Player player) {
		super();
		this.player = player;
	}
	//攻击方法
	protected void attack(Player p) {
		System.out.println("玩家"+player.getName()+"使用"+this.wName+"对玩家"+p.getName()+"攻击,造成"+this.arrackValue+"点伤害");
		p.setLifeValue(p.getLifeValue()-this.arrackValue);
	}
	@Override
	public String toString() {
		return "Weapon [player=" + player.getName() + ", arrackValue=" + arrackValue + ", wName=" + wName + "]";
	}
	
}
