package com.kaifamiao.game01;

import java.util.Random;

public class Game {
	Random r = new Random();
	//游戏内容
	/**
	 * 
	 * @param p1 主动玩家
	 * @param p2 被动玩家
	 * k为随机操作（0获得武器和1攻击）
	 * w为随机获得武器的类型（0坦克1枪）
	 * x为随机使用武器库中武器类型
	 */
	public void gameIn(Player p1, Player p2) {
		if (p1.getLifeValue() >= 0) {
			System.out.println(p1.getName() + "的回合");
			int k = r.nextInt(2);
			if (k == 0) {
				System.out.println(p1.getName() + "选择获得武器");
				int w = r.nextInt(2);
				switch (w) {
				case 0:
					Weapon w1 = new Tank(p1, r.nextInt(500));
					System.out.println(p1.getName() + "获得武器" + w1);
					p1.showWps();	
					break;
				case 1:
					Weapon w2 = new Gun(p1, r.nextInt(500));
					System.out.println(p1.getName() + "获得武器" + w2);
					p1.showWps();
					break;
				}
			}
			if (k == 1) {
				System.out.println(p1.getName() + "选择攻击");
				p1.showWps();
				int x = r.nextInt(p1.getWps().length);
				if (p1.useWps(x) != null) {
					p1.useWps(x).attack(p2);
					p2.display();
				} else {
					System.out.println("没有武器，造成伤害为0");
					p2.display();
				}
			}
		}
	}
	/**
	 * r随机数
	 * p1玩家1
	 * p2玩家2
	 * g游戏对象
	 * number游戏回合
	 * @param args
	 */
	public static void main(String[] args) {
		Random r = new Random();
		Player p1 = new Player(1000, "张无忌");
		Player p2 = new Player(1000, "张三丰");
		Game g = new Game();
		int number = 0;
		while (p1.getLifeValue() >= 0 && p2.getLifeValue() >= 0) {
			number++;
			System.out.println("第" + number + "回合");
			switch (r.nextInt(2)) {
			case 0:
				System.out.println("1p先手");
				g.gameIn(p1, p2);
				System.out.println();
				g.gameIn(p2, p1);
				System.out.println("***************************************************************");
				break;
			case 1:
				System.out.println("2p先手");
				g.gameIn(p2, p1);
				System.out.println();
				g.gameIn(p1, p2);
				System.out.println("***************************************************************");
				break;
			}
		}
		if (p1.getLifeValue() > p2.getLifeValue()) {
			System.out.println(p2.getName() + "阵亡");
			System.out.println("恭喜" + p1.getName() + "胜利");
		} else {
			System.out.println(p1.getName() + "阵亡");
			System.out.println("恭喜" + p2.getName() + "胜利");
		}
	}

}
