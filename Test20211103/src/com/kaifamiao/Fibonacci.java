package com.kaifamiao;

import java.util.Scanner;

public class Fibonacci {
	
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		System.out.println("输入第N位斐波那契数请输入大于0的数");
		Scanner scanner=new Scanner(System.in);
		int n=scanner.nextInt();
		System.out.println(FibonacciList(n));
	}
	
	
	
	public static int  FibonacciList(int n) {	
		
		if(n==1) {
			return 0;
		}else if(n==2){
			return 1;
		}else {
			return FibonacciList(n-2)+FibonacciList(n-1);
		}
	}
}
