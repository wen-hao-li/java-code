package com.kaifamiao;

import java.lang.Math;

public class Triangle {
	private double side1;
	private double side2;
	private double side3;

	public Triangle(double side1, double side2, double side3) {
		super();
		if (validate(side1, side2, side3)) {
			this.side1 = side1;
			this.side2 = side2;
			this.side3 = side3;
		}

	}
	
	public boolean validate(double side12, double side22, double side32) {

		if (side12 + side22 > side32 & side12 + side32 > side22 & side22 + side32 > side12) {
			return true;
		}
		return false;
	}

	public void shape() {
		if (validate(side1, side2, side3)) {

			int sideShape = 0;
			int shape = 0;
			if (side1 == side2 | side1 == side3 | side2 == side3) {
				sideShape = 1;
				if (side1 == side2 && side1 == side3) {
					sideShape = 2;
				}
			}
			if (side1 == Math.sqrt(Math.pow(side2, 2) + Math.pow(side3, 2))
					|| side2 == Math.sqrt(Math.pow(side1, 2) + Math.pow(side3, 2))
					|| side3 == Math.sqrt(Math.pow(side2, 2) + Math.pow(side1, 2))) {
				shape = 1;
			}
			if (shape == 0) {
				switch (sideShape) {
				case 0:
					System.out.println("普通三角形");
					break;
				case 1:
					System.out.println("等腰三角形");
					break;
				case 2:
					System.out.println("等边三角形");
					break;
				}
			} else {
				switch (sideShape) {
				case 0:
					System.out.println("直角三角形");
					break;
				case 1:
					System.out.println("等腰直角三角形");
					break;
				}
			}
		} else {
			System.out.println("不是三角形");
			return;
		}
	}

	public double calculate() {
		double p = (side1 + side2 + side3) / 2;
		return Math.sqrt(p * (p - side1) * (p - side2) * (p - side3));

	}

	public double getSide1() {
		return side1;
	}

	public void setSide1(double side1) {
		this.side1 = side1;
	}

	public double getSide2() {
		return side2;
	}

	public void setSide2(double side2) {
		this.side2 = side2;
	}

	public double getSide3() {
		return side3;
	}

	public void setSide3(double side3) {
		this.side3 = side3;
	}

}
