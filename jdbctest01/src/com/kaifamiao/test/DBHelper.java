package com.kaifamiao.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class DBHelper <T>{
	Connection conn=null;
	PreparedStatement pst=null;
	//1.加载驱动
	static {
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	//2.连接数据库
	public Connection getConnection() {
		if(conn==null) {
			try {
				conn=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:KFM","kaifamiao","kaifamiao");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return conn;
	}
	//3.创建statement对象执行sql
	public PreparedStatement getPst(String sql) {
		try {
			pst=getConnection().prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pst;
	}
	//4.操作结果显示
	public void show(String sql,IRowMapper<T> mapper) {
		List<T> list = new ArrayList<>();
		try {
			ResultSet results=getPst(sql).executeQuery();
			while(results.next()) {
				list.add(mapper.rowMapper(results));
			}
			for(T li:list) {
				System.out.println(li);
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}
	//5.释放资源
	public void close() {
		try {
			if(pst!=null) {
				pst.close();
			}if(conn!=null) {
				conn.close();
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
}
