package com.kaifamiao.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.kaifamiao.entity.Citizens;
import com.kaifamiao.entity.Department;
import com.kaifamiao.entity.Employees;
import com.kaifamiao.entity.IdCard;

public class JDBCTest01 {
	static Connection conn = null;
	static PreparedStatement pst = null;
	public static void main(String[] args) {
		
		try {
			Class.forName("oracle.jdbc.OracleDriver");
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:KFM", "kaifamiao", "kaifamiao");
			String sql = "select c.cname as 姓名,i.id as 身份证号 from t_citizens c join t_id_cards i on c.cards_id=i.id where c.cname= ?";
			pst = conn.prepareStatement(sql);
			System.out.println("输入查询的公民姓名");
			Scanner s = new Scanner(System.in);
			String name = s.next();
			pst.setObject(1, name);
			ResultSet rs = pst.executeQuery();
			Map<Citizens, IdCard> map = new HashMap<>();
			while (rs.next()) {
				Citizens c = new Citizens();
				IdCard i = new IdCard();
				c.setcName(rs.getNString("姓名"));
				i.setId(rs.getNString("身份证号"));
				map.put(c, i);
			}
			for (Map.Entry<Citizens, IdCard> entry : map.entrySet()) {
				System.out.println("姓名:" + entry.getKey().getcName() + "\t" + "身份证号为:" + entry.getValue().getId());
			}
			sql = "select c.cname as 姓名,i.id as 身份证号 from t_citizens c join t_id_cards i on c.cards_id=i.id where i.id= ?";
			pst.close();
			pst = conn.prepareStatement(sql);
			System.out.println("输入查询的身份证号码");
			String id = s.next();
			pst.setObject(1, id);
			
			rs = pst.executeQuery();
			map.clear();
			while(rs.next()) {
				Citizens c = new Citizens();
				IdCard i = new IdCard();
				c.setcName(rs.getNString("姓名"));
				i.setId(rs.getNString("身份证号"));
				map.put(c, i);
			}
			for (Map.Entry<Citizens, IdCard> entry : map.entrySet()) {
				System.out.println("姓名:" + entry.getKey().getcName() + "\t" + "身份证号为:" + entry.getValue().getId());
			}
			sql = "SELECT e.NAME 姓名 , d.NAME 部门名 FROM T_EMPLOYEES e JOIN T_DEPARTMENTS d ON e.DEPT_ID =d.ID WHERE d.ID =?";
			pst.close();
			pst = conn.prepareStatement(sql);
			System.out.println("输入查询的部门号,输出对应部门的员工");
			int deptid = s.nextInt();
			pst.setObject(1, deptid);
			
			rs = pst.executeQuery();
			Map<Employees,Department> map2=new HashMap<>();
			while(rs.next()) {
				Employees e = new Employees();
				Department d = new Department();
				e.setName(rs.getNString("姓名"));
				d.setName(rs.getNString("部门名"));
				map2.put(e,d);
			}
			for (Map.Entry<Employees, Department> entry : map2.entrySet()) {
				System.out.println("姓名:" + entry.getKey().getName() + "\t" + "部门名:" + entry.getValue().getName());
			}
			sql = "SELECT e.NAME 姓名 , d.NAME 部门名 FROM T_EMPLOYEES e JOIN T_DEPARTMENTS d ON e.DEPT_ID =d.ID WHERE e.name =?";
			pst.close();
			pst = conn.prepareStatement(sql);
			System.out.println("输入查询的员工名,输出对应的部门");
			String ename = s.next();
			pst.setObject(1, ename);
			
			rs = pst.executeQuery();
			map2.clear();
			while(rs.next()) {
				Employees e = new Employees();
				Department d = new Department();
				e.setName(rs.getNString("姓名"));
				d.setName(rs.getNString("部门名"));
				map2.put(e,d);
			}
			for (Map.Entry<Employees, Department> entry : map2.entrySet()) {
				System.out.println("姓名:" + entry.getKey().getName() + "\t" + "部门名:" + entry.getValue().getName());
			}
			sql = "INSERT INTO T_DEPARTMENTS (ID,NAME) VALUES (?,?)";
			pst.close();
			
			pst = conn.prepareStatement(sql);
			System.out.println("输入插入部门ID");
			
			deptid = s.nextInt();
			System.out.println("输入部门名");
			String deptname=s.next();
			pst.setObject(1, deptid);
			pst.setObject(2, deptname);
			int i=pst.executeUpdate();
			if(i==0) {
				System.out.println("插入失败");
			}else {
				System.out.println("插入成功");
			}
			sql = "INSERT INTO KAIFAMIAO.T_EMPLOYEES (ID,NAME,DEPT_ID) VALUES (1005,'阿敏',40)";
			insert(sql);
			sql = "INSERT INTO KAIFAMIAO.T_EMPLOYEES (ID,NAME,DEPT_ID) VALUES (1006,'张三',40)";
			insert(sql);
			sql = "INSERT INTO KAIFAMIAO.T_EMPLOYEES (ID,NAME,DEPT_ID) VALUES (1007,'李四',40)";
			insert(sql);
			sql = "INSERT INTO KAIFAMIAO.T_EMPLOYEES (ID,NAME,DEPT_ID) VALUES (1008,'王五',40)";
			insert(sql);
			sql = "INSERT INTO KAIFAMIAO.T_EMPLOYEES (ID,NAME,DEPT_ID) VALUES (1009,'赵六',40)";
			insert(sql);
			s.close();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if (pst != null) {
					System.out.println("关闭pst资源");
					pst.close();
				}
				if (conn != null) {
					System.out.println("关闭conn资源");
					conn.close();
				}
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

	}
	private static void insert(String sql) throws SQLException {
		pst.close();
		pst = conn.prepareStatement(sql);
		int i=pst.executeUpdate();
		if(i>0) {
			System.out.println("插入成功");
		}else {
			System.out.println("插入失败");
		}
	}

}
