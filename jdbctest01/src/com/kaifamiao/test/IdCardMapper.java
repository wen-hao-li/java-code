package com.kaifamiao.test;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.kaifamiao.entity.IdCard;

public class IdCardMapper implements IRowMapper<IdCard>{

	@Override
	public IdCard rowMapper(ResultSet result) throws SQLException {
		IdCard idcard=new IdCard();
		idcard.setId(result.getNString("id"));
		idcard.setAddress(result.getNString("address"));
		idcard.setBirthDate(result.getDate("birthdate"));
		idcard.setGender(result.getNString("gender"));
		idcard.setNation(result.getNString("nation"));
		idcard.setOffice(result.getNString("office"));
		return idcard;
	}

}
