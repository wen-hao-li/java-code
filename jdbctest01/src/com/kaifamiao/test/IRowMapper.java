package com.kaifamiao.test;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IRowMapper<T> {
	T rowMapper(ResultSet result)throws SQLException;
}
