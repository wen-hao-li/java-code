package com.kaifamiao.test;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.kaifamiao.entity.Citizens;

public class CitizenRowMapper implements IRowMapper<Citizens>{

	@Override
	public Citizens rowMapper(ResultSet result) throws SQLException {
		Citizens c=new Citizens();
		c.setCard_id(result.getNString("cards_id"));
		c.setcId(result.getLong("cid"));
		c.setcName(result.getString("cname"));
		c.setNationality(result.getString("nationality"));
		return c;
	}

}
