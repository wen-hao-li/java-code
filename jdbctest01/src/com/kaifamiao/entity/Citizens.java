package com.kaifamiao.entity;

public class Citizens {
	private String card_id;
	private long cId;
	private String cName;
	private String nationality;
	public String getCard_id() {
		return card_id;
	}
	public void setCard_id(String card_id) {
		this.card_id = card_id;
	}
	public long getcId() {
		return cId;
	}
	public void setcId(long cId) {
		this.cId = cId;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	@Override
	public String toString() {
		return "Citizens [card_id=" + card_id + ", cId=" + cId + ", cName=" + cName + ", nationality=" + nationality
				+ "]";
	}
	
}
